<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (isset($_REQUEST['sortby']))  {  
$sortby = $_REQUEST['sortby']; }
else {
$sortby = 'RollNumber'; }
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = "SELECT * FROM Citipix ORDER BY $sortby";
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UCSB Map and Imagery Lab - Citipix Collections</title>
<style type="text/css">
<!--
body {
	background-color: #A2ACD5;
}
a:link {
	color: #0000FF;
}
a:visited {
	color: #0000FF;
}
-->
</style>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>

<body>
<p><a href="index.php"><strong>Home</strong></a></p>
<table border="1" cellpadding="5" bgcolor="#E0E2EB">
  <tr>
    <td><a href="all_fields.php?sortby=FlightDate"><strong>Flight Date</strong></a></td>
    <td><strong><a href="all_fields.php?sortby=RollNumber">Roll Number</a></strong></td>
    <td><a href="all_fields.php?sortby=State"><strong>State</strong></a></td>
    <td><a href="all_fields.php?sortby=City"><strong>City</strong></a></td>
    <td><a href="all_fields.php?sortby=RangeFrom"><strong>Range From</strong></a></td>
    <td><a href="all_fields.php?sortby=RangeTo"><strong>Range To</strong></a></td>
    <td><a href="all_fields.php?sortby=NumberOfFrames"><strong>Number Of Frames</strong></a></td>
  </tr>
  <?php do { ?>
    <tr>
      <td><input name="id" type="hidden" id="id" value="<?php echo $row_Recordset1['id']; ?>" />
      <?php echo $row_Recordset1['FlightDate']; ?></td>
      <td><?php echo $row_Recordset1['RollNumber']; ?></td>
      <td><?php echo $row_Recordset1['State']; ?></td>
      <td><?php echo $row_Recordset1['City']; ?></td>
      <td><?php echo $row_Recordset1['RangeFrom']; ?></td>
      <td><?php echo $row_Recordset1['RangeTo']; ?></td>
      <td><?php echo $row_Recordset1['NumberOfFrames']; ?></td>
    </tr>
    <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>
</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
