<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Recordset1 = "ca";
if (isset($_GET['state'])) {
  $colname_Recordset1 = $_GET['state'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT city, state FROM cities WHERE state = %s ORDER BY city ASC", GetSQLValueString($colname_Recordset1, "text"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
$id = $_REQUEST['id'];
$RollNumber = $_REQUEST['RollNumber'];
$state = $_REQUEST['state'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UCSB Map and Imagery Lab - Citipix Collections</title>
<style type="text/css">
<!--
body {
	background-color: #A2ACD5;
}
a:link {
	color: #0000FF;
}
a:visited {
	color: #0000FF;
}
.style25 {color: #FFFFFF}
.style26 {color: #FF0000; }
-->
</style>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>

<body>
<p align="center" class="style25">Current RollNumber = <?php echo $RollNumber; ?></p>
<?php if ($totalRows_Recordset1 < 1) { // Show if recordset is empty ?>
  <p align="center" class="style26">There are no cities in our database for <?php echo $state; ?>.  You can continue without adding a city or <a href="add_state.php?id=<?php echo $id; ?>&amp;RollNumber=<?php echo $RollNumber; ?>">go back</a> and try again.</p>
  <?php } // Show if recordset is empty ?>
<?php if ($totalRows_Recordset1 > 0) { // Show if recordset not empty ?>
  <p align="center" class="style25">Now please select a city in <?php echo $state; ?>:</p>
  <?php } // Show if recordset not empty ?>
<form id="form1" name="form1" method="post" action="update_state_city.php">
  <p align="center" class="style25">
    <select name="city" id="city">
      <option value="">Please select</option>
      <?php
do {  
?>
      <option value="<?php echo $row_Recordset1['city']?>"><?php echo $row_Recordset1['city']?></option>
      <?php
} while ($row_Recordset1 = mysql_fetch_assoc($Recordset1));
  $rows = mysql_num_rows($Recordset1);
  if($rows > 0) {
      mysql_data_seek($Recordset1, 0);
	  $row_Recordset1 = mysql_fetch_assoc($Recordset1);
  }
?>
    </select>
    <input name="id" type="hidden" id="id" value="<?php echo $id; ?>" />
    <input name="RollNumber" type="hidden" id="RollNumber" value="<?php echo $RollNumber; ?>" />
    <input name="state" type="hidden" id="state" value="<?php echo $state; ?>" />
</p>
  <p align="center">
    <span class="style25">
    <input type="submit" name="EnterRecord" id="EnterRecord" value="Enter Record" />
  </span></p>
</form>
<p>&nbsp; </p>
</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
