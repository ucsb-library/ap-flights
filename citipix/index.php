<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (isset($_REQUEST['sortby']))  {  
$sortby = $_REQUEST['sortby']; }
else {
$sortby = 'RollNumber'; }
if ($sortby == 'City') {
  $sortby = 'City, State';
}
if ($sortby == 'State') {
  $sortby = 'State, City';
}
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = "SELECT id, RollNumber, `State`, City FROM Citipix ORDER BY $sortby ";
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

$queryString_Recordset1 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Recordset1") == false && 
        stristr($param, "totalRows_Recordset1") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Recordset1 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_Recordset1 = sprintf("&totalRows_Recordset1=%d%s", $totalRows_Recordset1, $queryString_Recordset1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UCSB Map and Imagery Lab - Citipix Collections</title>
<style type="text/css">
<!--
.style14 {font-size: small}
body {
	background-color: #A2ACD5;
}
.style18 {
	font-size: x-small;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
}
.style19 {
	color: #FFFFFF;
	font-family: Arial, Helvetica, sans-serif;
}
.style23 {font-family: Arial, Helvetica, sans-serif}
a:link {
	color: #0000FF;
}
.style24 {color: #000000; font-family: Arial, Helvetica, sans-serif; font-weight: bold; }
a:visited {
	color: #0000FF;
}
.style25 {font-size: x-small}
-->
</style>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>

<body>
<h2 align="center" class="style19">Citipix Rolls - Add or Update</h2>
<table width="100" border="1" align="center" bgcolor="#FFFFFF">
  <tr>
    <td><div align="center"><a href="add_roll.php" class="style18">Add new roll</a></div></td>
  </tr>
</table>
<br />
<table width="500" border="1" align="center" cellpadding="5" bgcolor="#FFFFFF">
  
  <tr class="style14">
    <td><a href="index.php?sortby=RollNumber" class="style24">Roll Number</a></td>
    <td><a href="index.php?sortby=State" class="style24">State</a></td>
    <td><a href="index.php?sortby=City" class="style24">City</a></td>
  </tr>
  <?php do { ?>
    <tr class="style14">
      <td><span class="style23">
        <input name="id" type="hidden" id="id" value="<?php echo $row_Recordset1['id']; ?>" />
        <a href="add_state.php?RollNumber=<?php echo $row_Recordset1['RollNumber']; ?>"><?php echo $row_Recordset1['RollNumber']; ?></a></span></td>
      <td><?php echo $row_Recordset1['State']; ?></td>
      <td><?php echo $row_Recordset1['City']; ?></td>
    </tr>
    <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>
<p align="center" class="style25"><a href="all_fields.php" class="style14">View all fields in database</a></p>
</body>
</html>
<?php
mysql_free_result($Recordset1);

mysql_free_result($Recordset1);
?>
