<!DOCTYPE html>
<html lang="en">
<head>
  <title>Find Aerial Photography by Location</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <?php include("common_code/include_MIL_all_style_links.php"); ?>
  <?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>
<body>
<?php include("common_code/include_MIL_header.php"); ?>
<br><br>
<table class="MILbottom-margin-20 MILlink" width="800" border="0" align="left" cellpadding="5" bordercolor="#000000">
  <tr>
    <td width="711"><div align="center">
      <table width="621" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td width="611" bgcolor="#FFFFFF"><div align="center" class="MILfont-x-large">
            <div align="left">
              Find Aerial Photography by Location
<br><br>

                  <table border="0">
                    <tr>
                      <td><ul class="MILfont-medium">
                          <li class="MILlist-no-disc">
                            <a href="california-counties.php"> Aerial Photography Flights by County </a>  </li>
                          <li class="MILlist-no-disc"><a href="ap_indexes/county.php?county_id=185&state_id=5&report_type=AllCa"> All California Flights </a>  </li>
                          <li class="MILlist-no-disc"><a href="ap_indexes/county_new.php?county_id=185&state_id=5&report_type=AllNonCa"> All other (non-California) Flights </a> </li>
                          <li class="MILlist-no-disc"><a href="ap_indexes/county.php?county_id=185&state_id=5&report_type=AllUS"> All U.S. Flights </a>  </li>
                          <li class="MILlist-no-disc"><a href="ap_indexes/county_new.php?county_id=185&state_id=5&report_type=AllNonUS"> Foreign Flights </a> </li>
                          <li class="MILlist-no-disc"><a href="ap_indexes/county.php?county_id=185&state_id=5&report_type=AllFlights">All flights </a> </li>
                        </ul>
                        <p><hr></p>
                        <ul class="depricated">
                        <li class="MILlist-no-disc"><a href="ap_flights/">Aerial Photography Cataloging System (login required)</a></li>
                        <li class="MILlist-no-disc"><a href="citipix/">Citipix Cataloging System  (login required)</a></li>
                        </ul>
                      </td>
                    </tr>
                  </table>
                  </div>
            </div>
              </td>
          </tr>
        </table>
              </td>
          </tr>
        </table>
<div class="MILabsolute-footer">
<?php include("common_code/include_MIL_footer.php"); ?>
</div>
</body>
</html>
