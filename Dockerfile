FROM registry.gitlab.com/ucsb-library/ap-flights/php:5.3-apache

COPY . /var/www/html/apcatalog/

RUN sed -i 's/^Mutex.*/#&/g; s/^PidFile.*/#&/g' "/etc/apache2/apache2.conf"
