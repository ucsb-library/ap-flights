<!DOCTYPE html>
<html lang="en">
<head>
  <title>Find Aerial Photography by Central County UCSB Library</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <?php include("common_code/include_MIL_all_style_links.php"); ?>
  <?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>
<body>
<?php include("common_code/include_MIL_header.php"); ?>
<br><br>
<table class="MILbottom-margin-20 MILlink" width="" border="0" align="left" cellpadding="5" bordercolor="#000000">
  <tr>
    <td width="711">
      <div align="center">
        <div>
          <p>Click on the map to see a list of aerial photography for that county.</p>
          <p>
          <img 
            alt="Clickable map of California counties, equal to the alphabetical list below." 
            class="imagecache-large_align_left" 
            src="images/images_counties/cal_inner_counties.jpg" 
            title="Central California Counties" 
            usemap="#Map" />
          </p>
        </div>
    </td>
<?php include("california-counties-text-list.html") ?>
  </tr>
</table>
<div class="MILabsolute-footer">
<?php include("common_code/include_MIL_footer.php"); ?>
</div>
<?php include("central-california-counties-imagemap.html"); ?>
</body>
</html>
