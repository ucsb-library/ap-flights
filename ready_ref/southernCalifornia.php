<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
   <title>Southern California Frequently Requested Flights</title>
   <meta name="keywords" content="Kern, Los Angeles, Ventura, Santa Barbara, San Luis Obispo, Bakersfield, San Diego, Riverside, San Bernardino, Orange">
   <meta name="description" content="These ready reference aids contain a sub-set of our flight holdings covering areas that are frequently requested by patrons - Kern, Los Angeles, Ventura, Santa Barbara, San Luis Obispo, Bakersfield, San Diego, Riverside, San Bernardino and Orange Counties. We have tried to select the best flights over a range of years for each area listed.">

<?php
include("../common_code/include_MIL_all_style_links.php");
?>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>
<body text="#000000" bgcolor="#FFFFFF" >

<?php
include("../common_code/include_MIL_header.php");
?>


<!--
<center><b><font face="Arial, Helvetica, sans-serif"><font size=+1>Ready Reference Aids for Air Photos</font></b></center>
<p> These ready reference aids contain a <b>sub-set</b> of our flight holdings covering areas that are frequently requested by patrons. We have tried to select the best flights over a range of years for each area listed.
-->

<div class="MILleft-margin-40 MILtop-margin-10 MILfont-x-large-bold">
<br>
Frequently Requested Flights of Southern California
</div>

<table class="MILleft-margin-40 MILtop-margin-10" ><tr>
<td valign="top"><img src="So_Cal_ready_ref_counties.jpg" width="601" height="361" border="0" usemap="#SoCalCounties" />
  <map name="SoCalCounties" id="SoCalCounties">
    <area shape="poly" coords="194,128,195,128,141,51,135,24,26,22,24,52,66,53,91,119" href="county.php?County=SAN LUIS OBISPO" alt="San Luis Obispo" />
    <area shape="poly" coords="195,132,53,120,53,120,48,180,192,177" href="county.php?County=SANTA BARBARA" alt="Santa Barbara" />
    <area shape="poly" coords="200,132,199,181,133,184,137,211,209,213,226,200,252,181,236,134" href="county.php?County=VENTURA" />
    <area shape="poly" coords="147,43,204,128,326,132,324,43" href="county.php?County=KERN" alt="Kern" />
    <area shape="poly" coords="243,136,259,183,221,211,266,229,287,230,299,206,314,206,325,184,322,137" href="county.php?County=LOS ANGELES" alt="Los Angeles" />
    <area shape="poly" coords="251,242,291,238,304,212,328,210,344,231,331,258,313,272,251,271" href="county.php?County=ORANGE" alt="Orange" />
    <area shape="poly" coords="328,203,563,189,587,169,547,106,463,39,330,45" href="county.php?County=SAN BERNARDINO" alt="San Bernadino" />
    <area shape="poly" coords="335,207,352,228,345,248,356,254,555,247,562,195" href="county.php?County=RIVERSIDE" alt="Riverside" />
    <area shape="poly" coords="342,256,314,279,361,331,445,321,439,256" href="county.php?County=SAN DIEGO" alt="San Diego" />
  </map>
</td>
</tr></table>

<div class="MILleft-margin-40 MILtop-margin-10 MILlink">
<a href="/apcatalog/ready_ref/northernCalifornia.php" onMouseOver="window.status='Frequently Requested Flights of Northern California'; return true">Frequently Requested Flights of Northern California</a>
<br>
&nbsp;
<br>
<a href="http://mil.library.ucsb.edu/ap_indexes/" onMouseOver="window.status='Aerial Photography Indexes by Flight ID'; return true">Aerial Photography Indexes listed by Flight ID</a>
<br>
&nbsp;
<br>
<a href="http://www.library.ucsb.edu/map-imagery-lab/california-aerial-photography-county" onMouseOver="window.status='Air Photo Flights listed by County'; return true">Aerial Photography Flights listed by County including access to indexes and information about the flights</a>
<br><br>
</div>

<?php
include("../common_code/include_MIL_footer.php");
?>



</body>
</html>
