<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Recordset1 = "502247";
if (isset($_GET['holding_id'])) {
  $colname_Recordset1 = $_GET['holding_id'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT Ready_Ref.holding_id, Ready_Ref.County,Ready_Ref.Region,  Ready_Ref.Format, Ready_Ref.Notes, Ready_Ref.Restrictions,Ready_Ref.image_name, ap_flights.filed_by, ap_flights.index_digital,ap_flights.scale_1,ap_flights.begin_date,ap_flights.digital,ap_flights.filmtype, bw,bw_IR,color, color_IR FROM ap_flights RIGHT JOIN Ready_Ref ON ap_flights.holding_id = Ready_Ref.holding_id WHERE Ready_Ref.holding_id = %s ORDER BY `begin_date` ASC", GetSQLValueString($colname_Recordset1, "int"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

$holding_id = $row_Recordset1['holding_id'];
?>
<?php  $lastTFM_nest = "";?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ready Ref Counties and Regions</title>
<style type="text/css">
<!--
.style1 {
	font-size: 18px;
	font-weight: bold;
}
.style2 {font-family: Arial, Helvetica, sans-serif}
.table { empty-cells:show; }
.style3 {font-size: small}

-->
</style>
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>

<body>
<table border="1" cellpadding="5">
  <tr>
    <td colspan="8" bgcolor="#FFFFFF" class="style1"><div align="center" class="style2">Ready Ref Listing for Flight <?php echo $row_Recordset1['filed_by']; ?></div></td>
  </tr>
  <tr>
    <td colspan="8" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="8" bgcolor="#CCCCCC"><div align="center" class="style1">
	<?php echo $row_Recordset1['County']; ?> COUNTY
    </div></td>
  </tr>
 
  <?php do { ?>
    <?php $TFM_nest = $row_Recordset1['Region'];
if ($lastTFM_nest != $TFM_nest) { 
	$lastTFM_nest = $TFM_nest; ?>
    <tr>
      <td colspan="8" bgcolor="#E0DFE3" class="style2" span><div align="center"><strong><br />
        <?php echo $row_Recordset1['Region']; ?></span></strong><br />
      </div></td>
    </tr>
    <tr bgcolor="#E0DFE3" class="style2">
    
    <td>Date</td>
    <td>Online Index</td>
    <td>Digital</td>
    <td>Scale</td>
    <td>Format</td>
    <td>Film Type</td>
    <td>Notes</td>
    <td>Restrictions</td>
  </tr>
    <?php } //End of Nested Repeat?>
    <tr>
      <td nowrap="nowrap" class="style3"><?php echo $row_Recordset1['begin_date']; ?>&nbsp;</td>
      <?php 
	$flight_id_clean = preg_replace('/\s\s+/', '', $row_Recordset1['filed_by']);
    $flight_id_cleaner = preg_replace('/[-._()]/', '', $flight_id_clean);
	$flight_id_lower = strtolower($flight_id_cleaner);
	?>
      <td class="style3">
        <font size="2">
        <?php if ($row_Recordset1['index_digital'] == 1) {  ?>
	    Index Available	    
	    <?php }
	else { ?>
	  Not Available
	  <?php
	}
	?>	  </td>
<td class="style3"><?php if ($row_Recordset1['digital'] == 1) {  ?>
	                             Yes<?php }
	                           else { ?>
	                             &nbsp;<?php } ?>	  </td>
      <td class="style3"><?php echo $row_Recordset1['scale_1']; ?>&nbsp;</td>
      <td class="style3"><?php echo $row_Recordset1['Format']; ?>&nbsp;</td>
      <td class="style3"><?php 
					  if ($row_Recordset1['bw'] == 1)  { ?>
			    		BW <?php }
			  		  if ($row_Recordset1['color'] == 1)  {  ?>
			    		Color <?php }
			  		  if ($row_Recordset1['bw_IR'] == 1)  {    ?>
			    		BW Infrared <?php }
			  		  if ($row_Recordset1['color_IR'] == 1)  {   ?>
			    	    Color Infrared <?php } ?>
	  &nbsp;</td>
      <td class="style3"><?php echo $row_Recordset1['Notes'];?> &nbsp;</td>
      <td class="style3"><?php echo $row_Recordset1['Restrictions'];?> &nbsp;</td>
    </tr>
    <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>
</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
