<?php
require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (isset($_REQUEST['orderby']))  {
$orderby = $_REQUEST['orderby']; }
else {
$orderby = 'filed_by'; }

//*******************************************

if (isset($_REQUEST['sortby']))  {
    $sortby = $_REQUEST['sortby'];
    $order = $_REQUEST['order'];
}
else {
    $sortby = 'filed_by';
    $order = 'ASC';
}
//*  print_r ($_REQUEST);

//*  echo "<br> sortby is " . $sortby . " yyyy and  order is "  . $order . " yeh <br>";
//*  echo "<br> sortby is " . $sortby . " yyyy and  new_order is "  . $new_order_id . " yeh <br>";

$new_order_begin_date = 'DESC';
$new_order_filed_by = 'ASC';
$new_order_id = 'ASC';

if ($sortby == "begin_date")  {
    if ($order == 'DESC') {
        $new_order_begin_date = 'ASC';
        }
    else {
        $new_order_begin_date = 'DESC';
        }
}


if ($sortby == "filed_by")  {
    if ($order == 'DESC') {
        $new_order_filed_by = 'ASC';
        }
    else {
        $new_order_filed_by = 'DESC';
        }
}
if ($sortby == "id") {
    if ($order == 'DESC') {
        $new_order_id = 'ASC';
        }
    else {
        $new_order_id = 'DESC';
        }
}

//* echo "<br> sortby2 is " . $sortby2 . " yyyy and  order2 is "  . $order2 . " yeh <br>";
//*echo "<br> sortby is " . $sortby . " yyyy and  new_order_begin_date is "  . $new_order_begin_date . " yeh <br>";


//*****************************************************

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
//* $query_Recordset1 = "SELECT * FROM ap_flights where prod_test = 'test' ORDER BY $orderby ASC";

$query_Recordset1 = "select ap_flights.holding_id as id, filed_by,
filed_by_in_catalog, filed_by_in_collection, begin_date, index_digital,
frames_scanned, ready_ref, prod_test,
    group_concat(distinct country_values.country_id separator '&&') as country_code,
    group_concat(distinct country_values.country separator '&&') as country_name,
    group_concat(state_values.state_code separator '&&')  as state_code,
    group_concat(distinct state_values.state separator '&&') as state_name
from ap_flights
    left outer join ap_flights_loc_country ON ap_flights.holding_id=ap_flights_loc_country.holding_id
    left outer join country_values ON ap_flights_loc_country.country_id=country_values.country_id
    left outer join ap_flights_loc_state ON ap_flights.holding_id=ap_flights_loc_state.holding_id
    left outer join state_values ON ap_flights_loc_state.state_id=state_values.state_id
where prod_test='test'
group by id
ORDER BY $sortby $order";


$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>List of Draft Aerial Photography Catalog Records</title>


<?php
include("../common_code/include_MIL_style_links.php");
?>

<script src="/apcatalog/jquery/jquery.js"></script>

<!-- this is the script that is used for the persist header/persist area classes that are used -->
<!-- to make the static table headers for the long lists of records that are displayed -->
<script type="text/javascript" src="/apcatalog/common_code/include_update_table_headers.js"></script>

</head>

<body class="MILlight-grey MILlink" onResize="window.location=window.location;">

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<table border="0" width="100%" align="center" cellpadding="10" cellspacing="0">
  <tr>
    <td colspan=2 align="center" class="MILfont-x-large"><span class="MILfont-bold">Draft</span> Aerial Photography Catalog Records<br>
    </td>
  </tr>
</table>
<br />

<table class="MILwhite persist-area" width="100%"  border="1" cellpadding="5" cellspacing="0">
  <thead>
  <tr class="MILdark-grey MILfont-list persist-header">
    <th align="center"><a href="list_new.php?sortby=filed_by&amp;order=<?php echo $new_order_filed_by; ?>">Filed By</a><br>
    <span class="MILfont-x-small">(select record to see detailed report) </span></th>
    <th align="center" class="MILfont-bold MILline-height-120">Filed By<br>In Catalog </th>
    <th align="center" class="MILfont-bold MILline-height-120">Filed By<br>
    In Collection </th>
    <th align="center"><a href="list_new.php?sortby=id&amp;order=<?php echo $new_order_id; ?>">Holding ID</a> </th>



    <th align="center"><a href="list_new.php?sortby=begin_date&amp;order=<?php echo $new_order_begin_date; ?>">Begin Date</a></th>
    <th align="center" class="MILfont-bold MILline-height-120">Area</th>
    <th align="center" class="MILfont-bold MILline-height-120">Index<br>Scanned</th>
    <th align="center" class="MILfont-bold MILline-height-120">Frames<br>Scanned</th>
    <th align="center" class="MILfont-bold MILline-height-120">Edit</th>
    <th align="center" class="MILfont-bold MILline-height-120">Delete</th>
  </tr>
  </thead>

  <tbody>
  <?php do { ?>
  <tr class="MILfont-list">
    <td><div align="center" class="MILfont-list"><a href="staff_report/report_staff.php?filed_by=<?php echo $row_Recordset1['filed_by']; ?>"><?php echo $row_Recordset1['filed_by']; ?></a></div></td>
    <td><div align="center"><?php echo $row_Recordset1['filed_by_in_catalog']; ?></div></td>
    <td><div align="center"><?php echo $row_Recordset1['filed_by_in_collection']; ?></div></td>
    <td><div align="center"><?php echo $row_Recordset1['id']; ?></div></td>
    <?php
  // convert mysql date to php timestamp
  $phptimestamp = strtotime( $row_Recordset1['begin_date'] );
  // now format php timestamp
  $begin_date = date( 'Y-m-d ', $phptimestamp );
  ?>
    <td nowrap><div align="center"><?php echo $begin_date; ?></div></td>

  <td><div align="center" >
     <?php

           $countryCode = explode("&&",$row_Recordset1['country_code']);
           $numCountryCode = count($countryCode);
           $countryName = explode("&&",$row_Recordset1['country_name']);
           $stateCode = explode("&&",$row_Recordset1['state_code']);
           $numStateCode = count($stateCode);
           $stateName = explode("&&",$row_Recordset1['state_name']);

           if ($numCountryCode >1) {
           echo "International";
           }
           elseif (($numCountryCode == 1) && ($countryCode[0] != 1))
           {
           echo $countryName[0];
           }
           elseif ($numStateCode == 1)
           {
           echo $stateName[0];
           }
           else
           {
           echo implode($stateCode, "  ");
           }
      ?>
        &nbsp;
  </div></td>

  <?php
  $flight_id_clean = preg_replace('/\s\s+/', '', $row_Recordset1['filed_by']);
    $flight_id_cleaner = preg_replace('/[-._()]/', '', $flight_id_clean);
  $flight_id_lower = strtolower($flight_id_cleaner);
  ?>

<?php
include("../common_code/include_report_staff_column_index.php");
?>

  <td>
  <?php if ($row_Recordset1['frames_scanned'] == 1) {  ?>
  <div align="left">Yes
      <?php }
    else if ($row_Recordset1['frames_scanned'] == 2) {  ?>
  </span>
  <div align="left" class="MILfont-list">Some
      <?php }
  else { ?>
    No
    <?php
  }
  ?>
  </div></td>
    <td><div align="center"><a href="update_form.php?record_type=test&holding_id=<?php echo $row_Recordset1['id']; ?>">Edit</a></div></td>
    <td><div align="center"><a href="delete_confirm.php?holding_id=<?php echo $row_Recordset1['id']; ?>"><span class="MILfont-red">Delete</span></a></div></td>
  </tr>
  <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
  </tbody>
</table>

<?php
include("../common_code/include_staff_footer.php");
?>



</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
