<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Recordset1 = "1";
if (isset($_GET['id'])) {
  $colname_Recordset1 = $_GET['id'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = sprintf("SELECT * FROM coord_pairs WHERE id = %s", GetSQLValueString($colname_Recordset1, "-1"));
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>MIL AP Flights - Update Coord Pairs</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
body {
	background-color: #999999;
}
.style3 {font-size: large}
.style4 {font-size: 10px}
-->
</style>

</head>

<body>
<table width="57%"  border="2" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td bgcolor="#FFFFFF"><div align="center" class="style3">Update Coord Pairs for MIL Air Photo Flights</div></td>
  </tr>
</table>
<form method="post" name="form1" action="update_coords_mysql.php">

<!-- #########################  OLD ONE   ########################

  <table align="center">
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Holding_id:</span></td>
      <td>
      <input name="holding_id" type="text" id="holding_id" value="<?php echo $row_Recordset1['holding_id']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">
        <input name="id" type="hidden" id="id2" value="<?php echo $row_Recordset1['id']; ?>">
      Filed_by:</span></td>
      <td><input name="filed_by" type="text" id="filed_by" value="<?php echo $row_Recordset1['filed_by']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Polygon_number:</span></td>
      <td><input type="text" name="polygon_number" value="<?php echo $row_Recordset1['polygon_number']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_lat_dir:</span></td>
      <td><input type="text" name="coord1_lat_dir" value="<?php echo $row_Recordset1['coord1_lat_dir']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_lat_deg:</span></td>
      <td><input type="text" name="coord1_lat_deg" value="<?php echo $row_Recordset1['coord1_lat_deg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_lat_min:</span></td>
      <td><input type="text" name="coord1_lat_min" value="<?php echo $row_Recordset1['coord1_lat_min']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_long_dir:</span></td>
      <td><input type="text" name="coord1_long_dir" value="<?php echo $row_Recordset1['coord1_long_dir']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_long_deg:</span></td>
      <td><input type="text" name="coord1_long_deg" value="<?php echo $row_Recordset1['coord1_long_deg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord1_long_min:</span></td>
      <td><input type="text" name="coord1_long_min" value="<?php echo $row_Recordset1['coord1_long_min']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_lat_dir:</span></td>
      <td><input type="text" name="coord2_lat_dir" value="<?php echo $row_Recordset1['coord2_lat_dir']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_lat_deg:</span></td>
      <td><input type="text" name="coord2_lat_deg" value="<?php echo $row_Recordset1['coord2_lat_deg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_lat_min:</span></td>
      <td><input type="text" name="coord2_lat_min" value="<?php echo $row_Recordset1['coord2_lat_min']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_long_dir:</span></td>
      <td><input type="text" name="coord2_long_dir" value="<?php echo $row_Recordset1['coord2_long_dir']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_long_deg:</span></td>
      <td><input type="text" name="coord2_long_deg" value="<?php echo $row_Recordset1['coord2_long_deg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord2_long_min:</span></td>
      <td><input type="text" name="coord2_long_min" value="<?php echo $row_Recordset1['coord2_long_min']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_lat_dir:</span></td>
      <td><input type="text" name="coord3_lat_dir" value="<?php echo $row_Recordset1['coord3_lat_dir']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_lat_deg:</span></td>
      <td><input type="text" name="coord3_lat_deg" value="<?php echo $row_Recordset1['coord3_lat_deg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_lat_min:</span></td>
      <td><input type="text" name="coord3_lat_min" value="<?php echo $row_Recordset1['coord3_lat_min']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_long_dir:</span></td>
      <td><input type="text" name="coord3_long_dir" value="<?php echo $row_Recordset1['coord3_long_dir']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_long_deg:</span></td>
      <td><input type="text" name="coord3_long_deg" value="<?php echo $row_Recordset1['coord3_long_deg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord3_long_min:</span></td>
      <td><input type="text" name="coord3_long_min" value="<?php echo $row_Recordset1['coord3_long_min']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_lat_dir:</span></td>
      <td><input type="text" name="coord4_lat_dir" value="<?php echo $row_Recordset1['coord4_lat_dir']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_lat_deg:</span></td>
      <td><input type="text" name="coord4_lat_deg" value="<?php echo $row_Recordset1['coord4_lat_deg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_lat_min:</span></td>
      <td><input type="text" name="coord4_lat_min" value="<?php echo $row_Recordset1['coord4_lat_min']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_long_dir:</span></td>
      <td><input type="text" name="coord4_long_dir" value="<?php echo $row_Recordset1['coord4_long_dir']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_long_deg:</span></td>
      <td><input type="text" name="coord4_long_deg" value="<?php echo $row_Recordset1['coord4_long_deg']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right"><span class="style4">Coord4_long_min:</span></td>
      <td><input type="text" name="coord4_long_min" value="<?php echo $row_Recordset1['coord4_long_min']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">&nbsp;</td>
      <td><input type="submit" value="Update record"></td>
    </tr>
  </table>


 #########################  END OF OLD ONE   ########################  -->


  <!-- %%%%%%%%%%%%% NEW ONE   %%%%%%%%%%%%%%%%%%%% -->

  <table align="center">
      <tr        <tr valign="baseline">
            <td nowrap align="right"><div align="right"></div></td>
            <td>
              <div align="right">Holding_id:            </div></td>
        <td><div align="left">
        <input name="holding_id" type="text" id="holding_id" value="<?php echo $row_Recordset1['holding_id']; ?>" size="25"></div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr valign="baseline">
        <td nowrap align="right"><div align="right">
          <input name="id" type="hidden" id="id2" value="<?php echo $row_Recordset1['id']; ?>"></div></td>
        <td>
              <div align="right">Filed_by:            </div></td>
        <td><div align="left"><input name="filed_by" type="text" id="filed_by" value="<?php echo $row_Recordset1['filed_by']; ?>" size="25"> </div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

      <tr valign="baseline">
            <td nowrap align="right"><div align="right"></div></td>
            <td>
              <div align="right">Polygon_number:            </div></td>
        <td><div align="left"><input type="text" name="polygon_number" value="<?php echo $row_Recordset1['polygon_number']; ?>" size="5">
            </div></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
      </tr>

          <tr valign="baseline">
            <td colspan="6" align="right" nowrap><div align="center">
              <p><br>
                <br>
              </p>
            </div></td>
          </tr>
          <tr valign="baseline">
            <td nowrap align="right"><div align="center" class="style11">Coord1_lat_dir</div></td>
            <td><div align="center" class="style11">Coord1_lat_deg</div></td>
            <td><div align="center" class="style11">Coord1_lat_min</div></td>
            <td><div align="center" class="style11">Coord1_long_dir</div></td>
            <td><div align="center" class="style11">Coord1_long_deg</div></td>
            <td><div align="center" class="style11">Coord1_long_min</div></td>
          </tr>

      <tr valign="baseline">
            <td nowrap align="right">
              <div align="center" class="style11">
        <input type="text" name="coord1_lat_dir" value="<?php echo $row_Recordset1['coord1_lat_dir']; ?>" size="5">
              </div></td>
            <td>
              <div align="center" class="style11">
        <input type="text" name="coord1_lat_deg" value="<?php echo $row_Recordset1['coord1_lat_deg']; ?>" size="25">
             </div></td>
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord1_lat_min" value="<?php echo $row_Recordset1['coord1_lat_min']; ?>" size="25">
             </div></td>
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord1_long_dir" value="<?php echo $row_Recordset1['coord1_long_dir']; ?>" size="5">
             </div></td>
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord1_long_deg" value="<?php echo $row_Recordset1['coord1_long_deg']; ?>" size="25">
             </div></td>
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord1_long_min" value="<?php echo $row_Recordset1['coord1_long_min']; ?>" size="25">
             </div></td>
      </tr>

          <tr valign="baseline">
            <td colspan="6" align="right" nowrap><div align="center" class="style11"><br>
            </div></td>
          </tr>

    <tr valign="baseline">
            <td nowrap align="right"><div align="center" class="style11">Coord2_lat_dir</div></td>
            <td><div align="center" class="style11">Coord2_lat_deg</div></td>
            <td><div align="center" class="style11">Coord2_lat_min</div></td>
            <td align="right" nowrap><div align="center" class="style11">Coord2_long_dir</div></td>
            <td><div align="center" class="style11">Coord2_long_deg</div></td>
            <td><div align="center" class="style11">Coord2_long_min</div></td>
          </tr>
          <tr valign="baseline">
            <td nowrap align="right">
              <div align="center" class="style11"><input type="text" name="coord2_lat_dir" value="<?php echo $row_Recordset1['coord2_lat_dir']; ?>" size="5">
             </div></td>
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord2_lat_deg" value="<?php echo $row_Recordset1['coord2_lat_deg']; ?>" size="25">
             </div></td>
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord2_lat_min" value="<?php echo $row_Recordset1['coord2_lat_min']; ?>" size="25">
             </div></td>
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord2_long_dir" value="<?php echo $row_Recordset1['coord2_long_dir']; ?>" size="5">
             </div></td>
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord2_long_deg" value="<?php echo $row_Recordset1['coord2_long_deg']; ?>" size="25">
             </div></td>
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord2_long_min" value="<?php echo $row_Recordset1['coord2_long_min']; ?>" size="25">
             </div></td>
             </tr>

          <tr valign="baseline">
            <td colspan="6" align="right" nowrap><div align="center" class="style11"><br>
            </div></td>
          </tr>
          <tr valign="baseline">
            <td nowrap align="right"><div align="center" class="style11">Coord3_lat_dir</div></td>
            <td><div align="center" class="style11">Coord3_lat_deg</div></td>
            <td><div align="center" class="style11">Coord3_lat_min</div></td>
            <td align="right" nowrap><div align="center" class="style11">Coord3_long_dir</div></td>
            <td><div align="center" class="style11">Coord3_long_deg</div></td>
            <td><div align="center" class="style11">Coord3_long_min</div></td>
          </tr>
          <tr valign="baseline">
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord3_lat_dir" value="<?php echo $row_Recordset1['coord3_lat_dir']; ?>" size="5"
              </div></td>
            <td>
              <div align="center" class="style11">
  <input type="text" name="coord3_lat_deg" value="<?php echo $row_Recordset1['coord3_lat_deg']; ?>" size="25">
              </div></td>
            <td>
              <div align="center" class="style11">
  <input type="text" name="coord3_lat_min" value="<?php echo $row_Recordset1['coord3_lat_min']; ?>" size="25">
              </div></td>
            <td>
              <div align="center" class="style11">
  <input type="text" name="coord3_long_dir" value="<?php echo $row_Recordset1['coord3_long_dir']; ?>" size="5">
              </div></td>
            <td>
              <div align="center" class="style11">
  <input type="text" name="coord3_long_deg" value="<?php echo $row_Recordset1['coord3_long_deg']; ?>" size="25">
              </div></td>
            <td>
              <div align="center" class="style11">
  <input type="text" name="coord3_long_min" value="<?php echo $row_Recordset1['coord3_long_min']; ?>" size="25">
              </div></td>
      </tr>

              <tr valign="baseline">
  	          <td colspan="6" align="right" nowrap><div align="center" class="style11"><br>
  	          </div></td>
          </tr>

  <tr valign="baseline">
            <td nowrap align="right"><div align="center" class="style11">Coord4_lat_dir</div></td>
            <td><div align="center" class="style11">Coord4_lat_deg</div></td>
            <td><div align="center" class="style11">Coord4_lat_min</div></td>
            <td align="right" nowrap><div align="center" class="style11">Coord4_long_dir</div></td>
            <td><div align="center" class="style11">Coord4_long_deg</div></td>
            <td><div align="center" class="style11">Coord4_long_min</div></td>
          </tr>
          <tr valign="baseline">
            <td nowrap align="right">
              <div align="center" class="style11">
  <input type="text" name="coord4_lat_dir" value="<?php echo $row_Recordset1['coord4_lat_dir']; ?>" size="5">
              </div></td>
            <td>
              <div align="center" class="style11">
  <input type="text" name="coord4_lat_deg" value="<?php echo $row_Recordset1['coord4_lat_deg']; ?>" size="25">
              </div></td>
            <td>
              <div align="center" class="style11">
  <input type="text" name="coord4_lat_min" value="<?php echo $row_Recordset1['coord4_lat_min']; ?>" size="25">
              </div></td>
            <td>
              <div align="center" class="style11">
  <input type="text" name="coord4_long_dir" value="<?php echo $row_Recordset1['coord4_long_dir']; ?>" size="5">
              </div></td>
            <td>
              <div align="center" class="style11">
  <input type="text" name="coord4_long_deg" value="<?php echo $row_Recordset1['coord4_long_deg']; ?>" size="25">
              </div></td>
            <td>
              <div align="center" class="style11">
  <input type="text" name="coord4_long_min" value="<?php echo $row_Recordset1['coord4_long_min']; ?>" size="25">
              </div></td>

      </tr>



      <tr valign="baseline">
        <td nowrap align="right">&nbsp;</td>
        <td><br><input type="submit" value="Update record"></td>
      </tr>
  </table>


  <!-- %%%%%%%%%%%%% END OF NEW ONE   %%%%%%%%%%%%%%%%%%%% -->

  <input type="hidden" name="holding_id" value="<?php echo $row_Recordset1['holding_id']; ?>">
</form>
<p align="center"><a href="list_coord_pairs.php">Return to Coordinate List without updating</a></p>
</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
