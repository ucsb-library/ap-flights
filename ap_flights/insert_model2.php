<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

 //*********************************8 testing
$all_selected_county_id=array();

//* 06152012 if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {

//* format location checkbox input values for insertion into database
$input_chkLocation = $_REQUEST['chkLocation'];
$location = implode(" ",$input_chkLocation);
//* for debugging ... echo "values in the chkLocation array are $location";

 	$county_state_country = $_REQUEST['chkCounty'];
 	$state_country = $_REQUEST['chkState'];
    $country = $_REQUEST['chkCountry'];

 	$input_country = $_REQUEST['chkCountry'];
 	$input_state = $_REQUEST['chkState'];
 	$input_county = $_REQUEST['chkCounty'];
 	$test_country = implode(" ",$input_country);
 	$test_state = implode(" ",$input_state);
 	$test_county = implode(" ",$input_county);

    $deduped_country = array_unique($input_country);
 	$test2_country = implode(" ",$deduped_country);

    $deduped_state_country = array_unique($input_state);
 	$test2_state = implode(" ",$deduped_state_country);

    $deduped_county_state_country = array_unique($input_county);
 	$test2_county = implode(" ",$deduped_county_state_country);


//* 0919  echo "<pre>";
//* 0919  ATTENTION : put this instruction in ONLY if want to see all variables and stay
//* 0919              in update_new_form screen ...  print_r($_POST);
//* 0919              Otherwise it will go where you tell it in the
//* 0919              header(sprintf("Location: %s", $updateGoTo));
//* 0919              instruction
//* 0919   echo "</pre>";

  $insertSQL = sprintf("INSERT INTO ap_flights (filed_by, official_flight_id, filed_by_in_catalog, filed_by_in_collection, location, special_location, scale_1, scale_2, scale_3, index_type, index_scale, index_filed_under, begin_date, end_date, `size`, height, width, vertical, oblique_high, oblique_low, overlap, sidelap, filmtype, bw, bw_IR, color, color_IR, spectral_range, printt, pos_trans, negative, digital, roll, cut_frame, filter, generation_held, camera, lens_focal_length, platform_id, directional_orientation, altitude_a, altitude_b, altitude_c, contractor_requestor, flown_by, acquired_from, note, copyright, access_limitations, area_general, counties, index_digital, frames_scanned, estimated_frame_count, ready_ref, prod_test) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['filed_by'], "text"),
                       GetSQLValueString($_POST['official_flight_id'], "text"),
                       GetSQLValueString($_POST['filed_by_in_catalog'], "text"),
                       GetSQLValueString($_POST['filed_by_in_collection'], "text"),
//                       GetSQLValueString($_POST['location'], "int"),
                       GetSQLValueString($location, "text"),
                       GetSQLValueString($_POST['special_location'], "text"),
                       GetSQLValueString($_POST['scale_1'], "int"),
                       GetSQLValueString($_POST['scale_2'], "int"),
                       GetSQLValueString($_POST['scale_3'], "int"),
                       GetSQLValueString($_POST['index_type'], "text"),
                       GetSQLValueString($_POST['index_scale'], "text"),
                       GetSQLValueString($_POST['index_filed_under'], "text"),
                       GetSQLValueString($_POST['begin_date'], "date"),
                       GetSQLValueString($_POST['end_date'], "date"),
                       GetSQLValueString($_POST['size'], "text"),
                       GetSQLValueString($_POST['height'], "double"),
                       GetSQLValueString($_POST['width'], "double"),
                       GetSQLValueString($_POST['vertical'], "int"),
                       GetSQLValueString($_POST['oblique_high'], "int"),
                       GetSQLValueString($_POST['oblique_low'], "int"),
                       GetSQLValueString($_POST['overlap'], "text"),
                       GetSQLValueString($_POST['sidelap'], "text"),
                       GetSQLValueString($_POST['filmtype'], "text"),
                       GetSQLValueString($_POST['bw'], "int"),
                       GetSQLValueString($_POST['bw_IR'], "int"),
                       GetSQLValueString($_POST['color'], "int"),
                       GetSQLValueString($_POST['color_IR'], "int"),
                       GetSQLValueString($_POST['spectral_range'], "text"),
                       GetSQLValueString($_POST['printt'], "int"),
                       GetSQLValueString($_POST['pos_trans'], "int"),
                       GetSQLValueString($_POST['negative'], "int"),
                       GetSQLValueString($_POST['digital'], "int"),
                       GetSQLValueString($_POST['roll'], "int"),
                       GetSQLValueString($_POST['cut_frame'], "int"),
                       GetSQLValueString($_POST['filter'], "text"),
                       GetSQLValueString($_POST['generation_held'], "text"),
                       GetSQLValueString($_POST['camera'], "text"),
                       GetSQLValueString($_POST['lens_focal_length'], "text"),
                       GetSQLValueString($_POST['platform_id'], "text"),
                       GetSQLValueString($_POST['directional_orientation'], "text"),
                       GetSQLValueString($_POST['altitude_a'], "text"),
                       GetSQLValueString($_POST['altitude_b'], "text"),
                       GetSQLValueString($_POST['altitude_c'], "text"),
                       GetSQLValueString($_POST['contractor_requestor'], "text"),
                       GetSQLValueString($_POST['flown_by'], "text"),
                       GetSQLValueString($_POST['acquired_from'], "text"),
                       GetSQLValueString($_POST['note'], "text"),
                       GetSQLValueString($_POST['copyright'], "text"),
                       GetSQLValueString($_POST['access_limitations'], "text"),
                       GetSQLValueString($_POST['area_general'], "text"),
                       GetSQLValueString($_POST['counties'], "text"),
                       GetSQLValueString($_POST['index_digital'], "int"),
                       GetSQLValueString($_POST['frames_scanned'], "int"),
                       GetSQLValueString($_POST['estimated_frame_count'], "int"),
                       GetSQLValueString($_POST['ready_ref'], "text"),
                       GetSQLValueString($_POST['prod_test'], "text"));

  mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
  $Result1 = mysql_query($insertSQL, $MilWebAppsdb1mysql) or die(mysql_error());

  //*  get last holding_id added to the ap_flights table

  $sql2 = "SELECT holding_id FROM ap_flights ORDER BY holding_id DESC LIMIT 1";
  $last_holding_id = @mysql_query($sql2,$MilWebAppsdb1mysql) or die(mysql_error());

  $row_last_holding_id = mysql_fetch_array($last_holding_id);
  $last_holding_id_added = $row_last_holding_id['holding_id'];


 //**** update the ap_flights_loc_counties table for that particular holding_id

//* method: add the updated information

 foreach($deduped_county_state_country as $item) {
     $spot = explode("xx",$item,3);
     $county_toadd = $spot[0];
     $state_toadd = $spot[1];
     $country_toadd = $spot[2];

     $sql_string1 = "insert ap_flights_loc_county set
     holding_id  = '$last_holding_id_added',
     country_id = '$country_toadd',
     state_id = '$state_toadd',
     county_id = '$county_toadd'";

 $result = @mysql_query($sql_string1,$MilWebAppsdb1mysql) or die(mysql_error());
 }

//**** update the ap_flights_loc_state table for that particular holding_id

//* method: add the updated information

  foreach($deduped_state_country as $item) {
      $spot = explode("xx",$item,2);
      $state_toadd = $spot[0];
      $country_toadd = $spot[1];

      $sql_string2 = "insert ap_flights_loc_state set
      holding_id  = '$last_holding_id_added',
      country_id = '$country_toadd',
      state_id = '$state_toadd'";

  $result = @mysql_query($sql_string2,$MilWebAppsdb1mysql) or die(mysql_error());
 }

//**** update the ap_flight_loc_countries table for that particular holding_id

//* method: add the updated information

 foreach($deduped_country as $item) {
     $deduped_country_toadd = $item;

     $sql_string4 = "insert ap_flights_loc_country set
      holding_id  = $last_holding_id_added,
      country_id = '$deduped_country_toadd'";
  $result = @mysql_query($sql_string4,$MilWebAppsdb1mysql) or die(mysql_error());
 }

//* special section ... what happens if user has deselected the united states but has not
//* deselected all the states within the united states (programme written so that when a state
//* is deselected, all the underlying counties are automatically deselected

$usa_id=1;
$usa_state = false;

foreach($deduped_state_country as $item) {
      $spot = explode("xx",$item,2);
      $country_toadd = $spot[1];
      if ($country_toadd == '1')
      {
      $usa_state = true;
      }
   }

if ($usa_state)  {
     if (in_array($usa_id,$deduped_country))
        {
//* 0919 for debugging           echo "<br> we have arrived <br>";
        }
     else {
          $sql_string5 = "insert ap_flights_loc_country set
          holding_id  = $last_holding_id_added,
          country_id = '$usa_id'";

          $result = @mysql_query($sql_string5,$MilWebAppsdb1mysql) or die(mysql_error());
     }
   }

$saved_record_type = $_REQUEST['record_type'];

if ($saved_record_type == 'test') {
    $updateGoTo = "list_new.php";
   }
   else {
    $updateGoTo = "list.php";
}

 if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }


  header(sprintf("Location: %s", $updateGoTo));

}

?>

<?php

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


$colname_model = "-1";
if (isset($_POST['filed_by'])) {
  $colname_model = $_POST['filed_by'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_model = sprintf("SELECT * FROM ap_flights WHERE filed_by = %s", GetSQLValueString($colname_model, "text"));
$model = mysql_query($query_model, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($model);
$totalRows_model = mysql_num_rows($model);

//* the default is to make this a test record
$record_type = "test";

$colname_location_id = "1";
//* get all the stored location(s) for a specific holding_id for prepopulation of insert screen data
$colname_location_id = $row_Recordset1['location'];
$location_id = (explode(" ",$colname_location_id));

//* get all the possible values for the location of the MIL collection
$query_location_values = "SELECT * FROM location_values ORDER BY sort_order ASC";
$location_values = mysql_query($query_location_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_location_values = mysql_fetch_assoc($location_values);
$totalRows_location_values = mysql_num_rows($location_values);

//* get all the stored country(s) for a specific holding_id for prepopulation of insert screen data
$colname_holding_id = $row_Recordset1['holding_id'];

//**** get all the countries for a particular holding_id
$query_holdings_country_values = sprintf("SELECT ap_flights_loc_country.country_id, country_values.country, country_values.country_sort_order FROM ap_flights_loc_country, country_values WHERE ap_flights_loc_country.holding_id = $colname_holding_id and ap_flights_loc_country.country_id = country_values.country_id order by country_values.country_sort_order", GetSQLValueString($colname_Recordset1, "int"));

$holdings_country_values = mysql_query($query_holdings_country_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_holdings_country_values = mysql_fetch_assoc($holdings_country_values);
$totalRows_holdings_country_values = mysql_num_rows($holdings_country_values);

//* get all the possible values for the countries of the MIL collection
$query_all_country_values = "SELECT * FROM country_values ORDER BY country_sort_order ASC";

$all_country_values = mysql_query($query_all_country_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_all_country_values = mysql_fetch_assoc($all_country_values);
$totalRows_all_country_values = mysql_num_rows($all_country_values);


//**** get all the states for a particular holding_id
$query_holdings_state_values = sprintf("SELECT ap_flights_loc_state.country_id, ap_flights_loc_state.state_id, state_values.state, state_values.country_id, state_values.state_sort_order, country_values.country FROM ap_flights_loc_state, state_values, country_values WHERE ap_flights_loc_state.holding_id = $colname_holding_id and ap_flights_loc_state.state_id = state_values.state_id and ap_flights_loc_state.country_id = state_values.country_id and ap_flights_loc_state.country_id = country_values.country_id order by state_values.state_sort_order", GetSQLValueString($colname_Recordset1, "int"));

$holdings_state_values = mysql_query($query_holdings_state_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
$totalRows_holdings_state_values = mysql_num_rows($holdings_state_values);

//* get all the possible values for the states of the MIL collection
$query_all_state_values = "SELECT * FROM state_values ORDER BY state_sort_order ASC";

$all_state_values = mysql_query($query_all_state_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_all_state_values = mysql_fetch_assoc($all_state_values);
$totalRows_all_state_values = mysql_num_rows($all_state_values);


//**** get all the counties for a particular holding_id
 $query_holdings_county_values = sprintf("SELECT ap_flights_loc_county.country_id, ap_flights_loc_county.state_id, ap_flights_loc_county.county_id, county_values.county, state_values.state, county_values.county_sort_order FROM ap_flights_loc_county, county_values, state_values WHERE ap_flights_loc_county.holding_id = $colname_holding_id and ap_flights_loc_county.county_id = county_values.county_id and ap_flights_loc_county.state_id = state_values.state_id order by county_values.county_sort_order", GetSQLValueString($colname_Recordset1, "int"));

 $holdings_county_values = mysql_query($query_holdings_county_values, $MilWebAppsdb1mysql) or die(mysql_error());
 $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
 $totalRows_holdings_county_values = mysql_num_rows($holdings_county_values);

 //* get all the states id and the name of the state for that particular holding - this may differ from the number of states
$query_holdings_countyState_values = sprintf("SELECT DISTINCT ap_flights_loc_county.state_id,  state_values.state FROM ap_flights_loc_county, county_values, state_values WHERE ap_flights_loc_county.holding_id = $colname_holding_id and ap_flights_loc_county.state_id = state_values.state_id order by state_values.state_sort_order", GetSQLValueString($colname_Recordset1, "int"));

  $holdings_countyState_values = mysql_query($query_holdings_countyState_values, $MilWebAppsdb1mysql) or die(mysql_error());
  $row_holdings_countyState_values = mysql_fetch_assoc($holdings_countyState_values);
  $totalRows_holdings_countyState_values = mysql_num_rows($holdings_countyState_values);

//* get all the possible values for the counties/parishes of the MIL collection
 $query_all_county_values = "SELECT * FROM county_values ORDER BY county_sort_order ASC";
 $all_county_values = mysql_query($query_all_county_values, $MilWebAppsdb1mysql) or die(mysql_error());
 $row_all_county_values = mysql_fetch_assoc($all_county_values);
 $totalRows_all_county_values = mysql_num_rows($all_county_values);

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Insert Draft Aerial Photography Catalog Record copied from: <?php echo $row_Recordset1['filed_by']; ?></title>


<script type="text/javascript">

//**** function to hide/unhide blocks of html code
 function unhide(divID) {
 var item = document.getElementById(divID);
 if (item) {
     item.className=(item.className=='hidden')?'unhidden':'hidden';
     }
 }
 //* end of function to hide/unhide blocks of html code

 //**** function to check/uncheck all input boxes
function checkAll(checkAllStateID,tickAll) {
    var elem = document.getElementById(checkAllStateID);
    var tickAll = arguments[1];
    var checkedCountry = arguments[0];
    var notUnitedStates = true;

	if (checkedCountry == 'checkAll1StateProvince')
	 {
	 notUnitedStates = false;
	 }

    var inputs = elem.getElementsByTagName('input');

    for (var i=0; i < inputs.length; i++)
     {
           if (inputs[i].checked && notUnitedStates) {
               inputs[i].checked = false;
               }

     }

   for (var i=0; i < inputs.length; i++)
      {
          if (tickAll == 'true') {
              inputs[i].checked = true;
              }
             else
            {
            if (notUnitedStates) {
 	      inputs[i].checked = false;
 	       }
          }
      }

  } //* end of function checkAll(checkAllStateID,tickAll)


//**** function anythingChecked unchecks all selected counties for a selected state
//**** and/or unchecks all selected state/provinces for a selected country
function anythingChecked(checkAllStateID) {
   var elem = document.getElementById(checkAllStateID);

//* do not allow user to uncheck all the states of the United States. Programme can
//* can wipe all the states that
//* user selected, but can not wipe all the underlying counties of all the underlying states
//* prgrammatically ... problem that!
   var checkedCountry = arguments[0];
   var notUnitedStates = true;
   if (checkedCountry == 'checkAll1StateProvince')
      {
       notUnitedStates = false;
      }

   var inputs = elem.getElementsByTagName('input');

   for (var i=0; i < inputs.length; i++)
       {
       if (inputs[i].checked && notUnitedStates) {
           inputs[i].checked = false;
       }

  }

} //* end of function anythingChecked(checkAllStateID)


</script>

<?php
include("../common_code/include_MIL_style_links.php");
?>

</head>

<body class="MILlight-grey">

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<table  width="100%"  border="0" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td class="MILfont-x-large"><div align="center" valign="middle">Insert <span class="MILfont-bold">Draft</span> Aerial Photography Catalog Record <br /></div>
    <div class="MILfont-large MILbottom-padding-10 MILline-height-150" align="center"> Copied from: <span class="MILfont-bold"><?php echo $row_Recordset1['filed_by']; ?></span> </div>
        </td>
  </tr>
</table>

<!-- **** start of the update form here **** -->
<form action="<?php echo $editFormAction; ?>" method="POST" name="form1">


  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="MILwhite">
    <tr>
      <td width="50%" valign="top" class="MILfont-edit MILleft-padding-40">

      <table width="480" border="1" align="left" cellspacing="0" cellpadding="5" bgcolor="#FFFFFF">


<tbody class="unhidden">

    <tr valign="baseline">
      <td width="133" align="right" nowrap class="MILfont-edit"><input name="holding_id2" type="hidden" id="holding_id2" value="<?php echo $row_Recordset1['holding_id']; ?>">
        Official flight id:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="official_flight_id" value="<?php echo $row_Recordset1['official_flight_id']; ?>" size="32"><br>
            Official full title of flight - first listed on <br>
            catalog record
            (called Flight I.D.)</td>
    </tr>

<?php
if ($record_type == 'test') {
?>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Filed by:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" name="filed_by" type="text" id="filed_by" value="<?php echo $row_Recordset1['filed_by']; ?>" size="32">
      <br>This field should only contain letters,<br>
          numbers and dashes (no other characters). </td>
    </tr>
<?php
   }
   else {
?>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Filed by:</td>
      <td class="MILfont-edit"><input name="filed_by" type="hidden" id="filed_by" value="<?php echo $row_Recordset1['filed_by']; ?>">
      Note: You must first change the record to &quot;test&quot; to change this field.</td>
    </tr>
<?php
}
?>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Filed by in catalog:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="filed_by_in_catalog" value="<?php echo $row_Recordset1['filed_by_in_catalog']; ?>" size="32"><br>
            Title flight is filed under in shelflist.</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Filed by in collection:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="filed_by_in_collection" value="<?php echo $row_Recordset1['filed_by_in_collection']; ?>" size="32"><br>
            Title flight is filed under in MIL <br>
            collection
            (usually the same as above, <br>
            except for PAI flights). </td>
    </tr>

</tbody>

 <!-- **** start of displaying country selection row on screen **** -->

<?php

//* set pointer to start of resultset array
	  $rows = mysql_num_rows($holdings_country_values);
	  if($rows > 0) {
 	      mysql_data_seek($holdings_country_values, 0);
      }

//* build string of all the country_ids that are selected for that particular holding_id
 while($row = mysql_fetch_assoc($holdings_country_values)){
      $resultset[] = $row; // fetch each row...
      $resultset2[] = $row['country_id']; // fetch each row...
      }

//* set pointer to start of array
	  $rows2 = mysql_num_rows($holdings_country_values);
	  if($rows2 > 0) {
	      mysql_data_seek($holdings_country_values, 0);
	  $row_holdings_country_values = mysql_fetch_assoc($holdings_country_values);
	  }

//*compose display variables for all country checkboxes
//*and from all country filed in database, indicate which ones have already been selected
	?>

<tbody class="unhidden">
<!-- new section Country/State/County starts here -->
	        <tr valign="baseline">
	          <td width="133" align="right" nowrap class="MILfont-edit">Country:</td>
			  <td class="MILfont-edit">

					<?php
	do  {
	?>

	<span class="MILlist-inline MILright-padding-5"><input type ="checkbox" onClick='anythingChecked("checkAll<?php echo $row_holdings_country_values['country_id'];?>StateProvince");' name="chkCountry[]" value="<?php echo $row_holdings_country_values['country_id'] ?>" checked >
	<?php echo $row_holdings_country_values['country'];?></span>

    <?php
    	} while ($row_holdings_country_values = mysql_fetch_assoc($holdings_country_values));

//*	  echo "I am above the here and the rows are $rows";

//*	  $rows = mysql_num_rows($row_holdings_country_values);
//*	  echo "I am here and the rows are $rows so xxxxxx";
//**************************echo "values in the chkLocation array are $location";
//*	  if($rows > 0) {
//*	      mysql_data_seek($row_holdings_country_values, 0);
//*		  $row_holdings_country_values = mysql_fetch_assoc($holdings_country_values);
//*	  }
	?>

<br><br>
	<span class="MILlink"><a href="javascript:unhide('displayAllCountries');">Display all Countries</a></span>

	<div id="displayAllCountries" class="hidden">

	<?php
	  do {

	?>
	<span class="MILlist-inline MILright-padding-5"><input type ="checkbox" onClick='unhide("display<?php echo $row_all_country_values['country_id'];echo "repeat";echo $row_all_country_values['country_id'];?>Country"); anythingChecked("checkAll<?php echo $row_all_country_values['country_id'];?>StateProvince");' name="chkCountry[]" value="<?php echo $row_all_country_values['country_id']?>" ><?php echo $row_all_country_values['country']?></span>

    <?php
	} while ($row_all_country_values = mysql_fetch_assoc($all_country_values));

//*	  $rows = mysql_num_rows($country_values);
//*	  if($rows > 0 ) {
//*	      mysql_data_seek($country_values, 0);
//*		  $row_all_country_values = mysql_fetch_assoc($all_country_values);
//*	  }

	?>

</div>
         </td>
     </tr>

 <!-- **** end of displaying COUNTRY selection row on screen **** -->

 <!-- *********************************************************** -->
 <!-- **** start of displaying STATE selection row on screen **** -->

 <!-- *** first display all the STATE information for that particular holding -->


<tbody class='unhidden'>

<?php
	 $rows222 = mysql_num_rows($holdings_state_values);
     if($rows222 > 0 ) {
        mysql_data_seek($holdings_state_values, 0);
        $row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
		  }

$current_country = $row_holdings_state_values['country'];
$current_country_id = $row_holdings_state_values['country_id'];
$totalRows_holdings_state_values;

$new_country = "abc";
$current_country = chop($current_country);

for ($ii=0; $ii<=$totalRows_holdings_state_values; $ii++)
  {  //* beginning of ii LOOP statement

   if ($current_country != $new_country or $new_country == "") { // ** new country found or end of read in holdings

   if ($ii==0) { //* beginning of ii IF statement
			  ?>
					  </td>
				  </tr>
			 <!-- end of the tbody for each country -->
			 </tbody >
			 <?php

			 $new_country = $current_country;
			 $new_country_id = $current_country_id;
     }
     else {
           $rows33 = mysql_num_rows($all_state_values);
           if($rows33 > 0) {
                  	      mysql_data_seek($all_state_values, 0);
                 		  $row_all_state_values = mysql_fetch_assoc($all_state_values);
                          $row_all_state_values['country_id'];
 	                      }
 	      ?>

<br><br>

    <a href="javascript:unhide('displayAll<?php echo $current_country_id;?>States');">Display all  States</a>

 	<div id="displayAll<?php echo $current_country_id;?>States" class="hidden">

		  <?php

		  do {                     //* doloop 1 starts here

		  if ($current_country_id == $row_all_state_values['country_id'])
			  { //* printout only that country
			  "row_all_state_values['state_id'] //* ";
			 ?>
			 <span class="MILlist-inline MILright-padding-5"><input type ="checkbox" onClick='unhide("display<?php echo $row_all_state_values['state_id'];?>Counties"); anythingChecked("checkAll<?php echo $row_all_state_values['state_id'];?>Counties");' name="chkState[]" value="<?php echo $row_all_state_values['state_id']; echo "xx";   echo $row_all_state_values['country_id']?>"  ><?php echo $row_all_state_values['state'];?></span>

			 <?php
			  } //* end of printout only that country

			} while ($row_all_state_values = mysql_fetch_assoc($all_state_values)); //* doloop 1 end here

		$rows = mysql_num_rows($all_state_values);
		if($rows > 0 ) {
			  mysql_data_seek($all_state_values, 0);
			  $row_all_state_values = mysql_fetch_assoc($all_state_values);

 	  } //* end  of ii IF statement

      ?>
     </td>
  </tr>

  <!-- end of the tbody for each country -->

<?php
}  //* end of new country found or end of read in holdings

$current_country = $new_country;
$current_country_id = $new_country_id;

if ($new_country != "")
    {  //** this is the last record of the selected holding... do not printout the heading for the next country

	 ?>
	  <tbody id="display<?php echo $row_holdings_state_values['country_id'];?>Country" class="unhidden">
	  <!-- beginning of the tbody for each country -->

	  <tr valign="baseline" id ="checkAll<?php echo $row_holdings_state_values['country_id'];?>StateProvince">
		 <td width="133" align="right" nowrap class="MILfont-edit"><?php echo $row_holdings_state_values['country']; echo ":"; ?>
		 </td>

		 <?php
		 //* do NOT allow mass addition of all states JUST for the United States case
		 //* as have click on individual states in order to show/unmask the underlying counties
		 //*
		 //* do NOT allow mass removal of all states JUST for the United States case
		 //* as have to remove each state to ensure that all underlying counties are
		 //* also automatically removed

		if ($row_holdings_state_values['country_id'] != 1) {
			?>
		 <td class="MILfont-edit"><a href="javascript:checkAll('checkAll<?php echo $row_holdings_state_values['country_id'];?>StateProvince', 'true');">Check all State/Province </a>
			 &nbsp; &nbsp; <a href="javascript:checkAll('checkAll<?php echo $row_holdings_state_values['country_id'];?>StateProvince', 'false');">Uncheck all State/Province</a><br>

			<?php
          }
     	else
     	{
		?>
		<td class="MILfont-edit">
		<?php
		}//* end of do not display the "Check all" or "Uncheck all" if the country is the United States

    }  //** end of this is the last record of the selected holding. do not printout the heading for the next country


}  // ** end of new country found

if ($new_country != "")
    {  //** this is the last record of the selected holding... do not printout any record for the next country
	?>

	<span class="MILlist-inline MILright-padding-5"><input type ="checkbox" onclick="anythingChecked('<?php echo $row_holdings_state_values['state']; echo $row_holdings_state_values['state_id'];?>');" name="chkState[]" value="<?php echo $row_holdings_state_values['state_id']; echo "xx"; echo $row_holdings_state_values['country_id']; ?>" checked><?php echo $row_holdings_state_values['state'];?></span>

	<?php

    }  //**  end of this is the last record of the selected holding. do not printout any record for the next country

$rows = mysql_num_rows($holdings_state_values);
if($rows > 0 ) {
	  $row_holdings_state_values = mysql_fetch_assoc($holdings_state_values);
	  $new_country = $row_holdings_state_values['country'];
	  $new_country_id = $row_holdings_state_values['country_id'];
	  $new_country = chop($new_country);
    }

} //* end of FOR ii LOOP statement


?>

           </td>
          </tr>
</div>

<!-- **** now display all the states in all the countries **** -->

  <?php

  $rows22more = mysql_num_rows($all_state_values);
  if($rows22more > 0 ) {
	      mysql_data_seek($all_state_values, 0);
	  $row_all_state_values = mysql_fetch_assoc($all_state_values);
  }

$current_country = $row_all_state_values['country'];
$current_id = $row_all_state_values['country_id'];
$new_country = " ";
$current_country = chop($current_country);

for ($counterii=0; $counterii<($totalRows_all_state_values); $counterii++)
  {

	if ($current_country != $new_country) { // ** new country found

		if ($counterii==0) { //* start of IF counter ii
		$new_country = $current_country;
		}
		else {
		?>
			</td>
		  </tr>

		  <!-- end of the tbody for each country -->
		  </tbody >

		<?php
		} //* end of IF counter ii

		$current_country = $new_country;
		?>

		<tbody id="display<?php echo $row_all_state_values['country_id']; echo "repeat"; echo $row_all_state_values['country_id'];?>Country" class="hidden">
		  <!-- beginning of the tbody for each country -->

		  <tr valign="baseline" id ="checkAll<?php echo $row_all_state_values['country_id'];?>StateProvince">
			 <td width="133" align="right" nowrap class="MILfont-edit"><?php echo $row_all_state_values['country']; echo ":";?>
			 </td>

       <?php
		 //* do NOT allow mass addition of all states JUST for the United States case
		 //* as have click on individual states in order to show/unmask the underlying counties
		 //*
		 //* do NOT allow mass removal of all states JUST for the United States case
		 //* as have to remove each state to ensure that all underlying counties are
		 //* also automatically removed

		if ($row_all_state_values['country_id'] != 1) {
			?>
			 <td class="MILfont-edit"><a href="javascript:checkAll('checkAll<?php echo $row_all_state_values['country_id'];?>StateProvince', 'true');">Check all State/Province</a>
			 &nbsp; &nbsp; <a href="javascript:checkAll('checkAll<?php echo $row_all_state_values['country_id'];?>StateProvince', 'false');">Uncheck all State/Province</a><br>

			<?php
          }
     	else
     	{
		?>
		<td class="MILfont-edit">
		<?php
		}//* end of do not display the "Check all" or "Uncheck all" if the country is the United States

	}  // ** end of new country found

?>

<span class="MILlist-inline MILright-padding-5"><input type ="checkbox" onClick='unhide("display<?php echo $row_all_state_values['state_id'];?>Counties"); anythingChecked("checkAll<?php echo $row_all_state_values['state_id'];?>Counties");' name=
"chkState[]" value="<?php echo $row_all_state_values['state_id']; echo "xx"; echo $row_all_state_values['country_id']; echo "xx"; echo $row_all_state_values['country_id']?>" ><?php echo $row_all_state_values['state'];?></span>

<?php

  $rows = mysql_num_rows($all_state_values);
  if($rows > 0 ) {
	  $row_all_state_values = mysql_fetch_assoc($all_state_values);
	  $new_country = $row_all_state_values['country'];
	  $new_country = chop($new_country);
  	  $new_county = $row_all_state_values['county'];
    }

} //* end of for counterii loop

?>
           </td>
          </tr>
</div>

<!-- **** end of now display all the states in all the countries **** -->

<!-- *********************************************** -->
<!-- **** this is the display of all the counties section **** -->

<!-- first display all the counties already enetered for that particular holding
     then build the display so that all counties for all states can be potentially displayed
     and data entered -->

<!-- **** display all the counties of all the states for that particular holding **** -->
    <?php
    $rows = mysql_num_rows($holdings_countyState_values);
    if($rows > 0) {
  	      mysql_data_seek($holdings_countyState_values, 0);
    }

//* need to wind array back to the beginning
//* set pointer to start of resultset array for the STATES for chosen flight_id
 	$rows77 = mysql_num_rows($holdings_countyState_values);
	if($rows77 > 0) {
  	    mysql_data_seek($holdings_countyState_values, 0);
    }

  $row_holdings_countyState_values = mysql_fetch_assoc($holdings_countyState_values);

//* set pointer to start of resultset array for the COUNTIES for chosen flight_id
 	$rows2 = mysql_num_rows($holdings_county_values);
 	if($rows2 > 0) {
 	     mysql_data_seek($holdings_county_values, 0);
   	  $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
 	}

//*compose display variables for all county checkboxes
//*and from all county filed in database, indicate which ones have already been selected

$new_state = " ";
$current_state = $row_holdings_countyState_values['state'];

for ($counter=0; $counter<$totalRows_holdings_countyState_values; $counter++)
   {

	if ($current_state != $new_state) {  //* new state found so create new state row for display

		if ($counter ==0) {
		$new_state = $current_state;
		}
		else {
		?>
			</td>
		  </tr>

		  <!-- end of the div for each state -->
		  </div>
		<?php
		}

		$current_state = $new_state;
		?>

		<tbody id="<?php echo trim($row_holdings_countyState_values['state']); ?>" class="unhidden" >
		  <!-- beginning of the tbody for each state -->


					<tr valign="baseline">
					  <td width="133" align="right" nowrap class="MILfont-edit"><?php echo $row_holdings_countyState_values['state']; ?> Counties:</td>
					  <td slign="left" id="<?php echo trim($row_holdings_countyState_values['state']); ?><?php echo trim($row_holdings_countyState_values['state_id']); ?>" class="MILfont-edit">
		<?php
	}   //* end of new state found so create new state row for display

 	do  {
        if ($row_holdings_county_values['state_id'] == $row_holdings_countyState_values['state_id'])
            {  //** print out all counties ticked for that state
 	?>

        	<span class="MILlist-inline MILright-padding-5"><input type ="checkbox" name="chkCounty[]" value="<?php echo $row_holdings_county_values['county_id']; echo "xx";echo $row_holdings_county_values['state_id']; echo "xx";   echo $row_holdings_county_values['country_id'] ?>" checked >
        	<?php echo $row_holdings_county_values['county'];?></span>

        	<?php
           } //** end of print out all counties ticked for that state


     	} while ($row_holdings_county_values = mysql_fetch_assoc($holdings_county_values));

    $rows38 = mysql_num_rows($holdings_county_values);
    if($rows38 > 0) {
       mysql_data_seek($holdings_county_values, 0);
       $row_holdings_county_values = mysql_fetch_assoc($holdings_county_values);
 	  }
 	?>
 <br><br>

    <a href="javascript:unhide('displayAll<?php echo $row_holdings_countyState_values['state']; ?> Counties');">Display all <?php echo $row_holdings_countyState_values['state']; ?> Counties</a>

	<div id="displayAll<?php echo $row_holdings_countyState_values['state']; ?> Counties" class="hidden">

 	<?php
  $printoutonce = "once";

  do {
     if ($row_all_county_values['state_id'] == $row_holdings_countyState_values['state_id'])
         {  //** print out absolutely all counties for that state

 	      if ($printoutonce == "once")
 	          { //** print out the check all counties/uncheck all counties options at the top
	            //** of the list of all counties display
	    		?>

	    		<span id ="checkAll<?php echo $row_all_county_values['state_id'];?>Counties">
	    		<a href="javascript:checkAll('checkAll<?php echo $row_all_county_values['state_id'];?>Counties', 'true');">Check all Counties</a>
	    		&nbsp; &nbsp; <a href="javascript:checkAll('checkAll<?php echo $row_all_county_values['state_id'];?>Counties', 'false');">Uncheck all Counties</a><br>
	    		<?php
	    		$printoutonce = "done";
              }
    	?>

         <span class="MILlist-inline MILright-padding-5"><input type ="checkbox" name="chkCounty[]" value="<?php echo $row_all_county_values['county_id']; echo "xx";   echo $row_all_county_values['state_id']; echo "xx";   echo $row_all_county_values['country_id']?>" ><?php echo $row_all_county_values['county'];?></span>

         <?php
         } //** end of print out absolutely all counties ticked for that state

    } while ($row_all_county_values = mysql_fetch_assoc($all_county_values));

    $rows = mysql_num_rows($all_county_values);
    if($rows > 0 ) {
       mysql_data_seek($all_county_values, 0);
       $row_all_county_values = mysql_fetch_assoc($all_county_values);
    }

  ?>

  </span>
         </td>
      </tr>
  </tbody>
  <?php

  $rows = mysql_num_rows($holdings_countyState_values);
  if($rows > 0 ) {
     $row_holdings_countyState_values = mysql_fetch_assoc($holdings_countyState_values);
    }

  $new_state = $row_holdings_countyState_values['state_id'];

} //*** end of counter loop

?>

</div>
       </td>
   </tr>

<!-- **** end of display all the counties of all the states for that particular holding **** -->

<!-- *********** beginning of added all the counties for all the states ************ -->

<?php

$current_state = $row_all_county_values['state'];
$current_id = $row_all_county_values['state_id'];
$new_state = " ";
$current_state = chop($current_state);

for ($ii=0; $ii<$totalRows_all_county_values; $ii++)
  {

	if ($current_state != $new_state) { // ** new state found
		?>
		<?php
			if ($ii==0) {
			   $new_state = $current_state;
			}
			else {
			   ?>
			   </td>
			   </tr>
			   <!-- end of the tbody for each state -->
			   </tbody >
			   <?php
			}
		 $current_state = $new_state;
		 ?>

		<tbody id="display<?php echo $row_all_county_values['state_id'];?>Counties" class="hidden">
		<!-- beginning of the tbody for each state -->
		<tr valign="baseline" id ="checkAll<?php echo $row_all_county_values['state_id'];?>Counties">
		   <td width="27%" align="right" nowrap class="MILfont-edit"><?php echo $row_all_county_values['state'];?>
		   Counties:</td>
		   <td class="MILfont-edit"><a href="javascript:checkAll('checkAll<?php echo $row_all_county_values['state_id'];?>Counties', 'true');">Check all Counties</a>
		   &nbsp; &nbsp; <a href="javascript:checkAll('checkAll<?php echo $row_all_county_values['state_id'];?>Counties', 'false');">Uncheck all Counties</a><br>
		<?php
	}  // ** end of new state found

   ?>
   <span class="MILlist-inline MILright-padding-5"><input type ="checkbox" name="chkCounty[]" value="<?php echo $row_all_county_values['county_id']; echo "xx"; echo $row_all_county_values['state_id']; echo "xx"; echo $row_all_county_values['country_id']?>" ><?php echo $row_all_county_values['county'];?></span>

   <?php
   $rows = mysql_num_rows($all_county_values);
   if($rows > 0 ) {
	  $row_all_county_values = mysql_fetch_assoc($all_county_values);
	  $new_state = $row_all_county_values['state'];
	  $new_state = chop($new_state);
  	  $new_county = $row_all_county_values['county'];
   }

} //* end of for ii FOR loop

?>
       </td>
    </tr>
</div>

<!-- *********** end of added all the counties for all the states ************ -->
<!-- ************************************************************************* -->

<tbody class="unhidden">

<?php
include("../common_code/include_ready_ref_yes_no.php");
?>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Begin date:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="begin_date" value="<?php echo $row_Recordset1['begin_date']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">End date:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="end_date" value="<?php echo $row_Recordset1['end_date']; ?>" size="32"><br>
              End date is not required if <br>
              flight
            has only one date. </td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Scale:</td>
      <td class="MILfont-edit">1:&nbsp;<input class="MILfont-input-box" type="text" name="scale_1" value="<?php echo $row_Recordset1['scale_1']; ?>" size="32"><br>
            Just enter number. Example: 12000 </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Scale:</td>
      <td class="MILfont-edit">1:&nbsp;<input class="MILfont-input-box" type="text" name="scale_2" value="<?php echo $row_Recordset1['scale_2']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Scale:</td>
      <td class="MILfont-edit">1:&nbsp;<input class="MILfont-input-box" type="text" name="scale_3" value="<?php echo $row_Recordset1['scale_3']; ?>" size="32"></td>
    </tr>


    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Overlap:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="overlap" value="<?php echo $row_Recordset1['overlap']; ?>" size="32"><br>
            Enter % overlap. Example:  60%. </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Sidelap:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="sidelap" value="<?php echo $row_Recordset1['sidelap']; ?>" size="32"><br>
            Enter % sidelap. Example:  20%. </td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Special location:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="special_location" value="<?php echo $row_Recordset1['special_location']; ?>" size="32"><br>
            Enter if imagery filed in unusual location: <br>
            Example: Annex 2 processing shelves </td>
    </tr>

<?php
include("../common_code/include_location_checkboxes.php");
?>


<?php
include("../common_code/include_frames_scanned_yes_no.php");
?>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Index type:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="index_type" value="<?php echo $row_Recordset1['index_type']; ?>" size="32"></td>
    </tr>
<?php
include("../common_code/include_index_digital_yes_no.php");
?>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Index scale:</td>
      <td class="MILfont-edit">1:&nbsp;<input class="MILfont-input-box" type="text" name="index_scale" value="<?php echo $row_Recordset1['index_scale']; ?>" size="32"><br>
             Example: 12000, 24000, 50000 </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Index filed under:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="index_filed_under" value="<?php echo $row_Recordset1['index_filed_under']; ?>" size="32"><br>
            If index filed under alternative name, <br>
            list that name here.</td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Estimated frame count:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="estimated_frame_count" value="<?php echo $row_Recordset1['estimated_frame_count']; ?>" size="32"><br>
    Number of frames held <br>
    (put in estimate if exact # not known). </td>
    </tr>


    </table></td>

    <td width="50%" valign="top" class="MILfont-edit MILleft-padding-15">
      <table width="480"  border="1" cellpadding="5"cellspacing="0" bgcolor="#FFFFFF">


    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Contractor requestor:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="contractor_requestor" value="<?php echo $row_Recordset1['contractor_requestor']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Flown by:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="flown_by" value="<?php echo $row_Recordset1['flown_by']; ?>" size="32"><br>
            Example: U.S. Geological Survey</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Acquired from:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="acquired_from" value="<?php echo $row_Recordset1['acquired_from']; ?>" size="32"><br>
            Example: U.S. Forest Service or <br>            Landiscor Aerial Information.</td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Copyright:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="copyright" value="<?php echo $row_Recordset1['copyright']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Access limitations:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="access_limitations" value="<?php echo $row_Recordset1['access_limitations']; ?>" size="32"></td>
    </tr>

<?php
include("../common_code/include_physical_fields_details_display.php");
?>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Size:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="size" value="<?php echo $row_Recordset1['size']; ?>" size="32"><br>
            Example: frames 9 x 9 inches or frames 70mm </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Height:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="height" value="<?php echo $row_Recordset1['height']; ?>" size="32"><br>
            List height in inches.</td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Width:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="width" value="<?php echo $row_Recordset1['width']; ?>" size="32"><br>
            List height in inches.</td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Generation held:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="generation_held" value="<?php echo $row_Recordset1['generation_held']; ?>" size="32"><br>
            Example: 3rd generation or <br>            1st and 2nd generation. </td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Directional orientation:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="directional_orientation" value="<?php echo $row_Recordset1['directional_orientation']; ?>" size="32"><br>
            Example: West-East; Northwest-Southeast             </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Platform id:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="platform_id" value="<?php echo $row_Recordset1['platform_id']; ?>" size="32"><br>
            Example: U-2 Aircraft #5 .              </td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Altitude:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="altitude_a" value="<?php echo $row_Recordset1['altitude_a']; ?>" size="32"><br>
            Example: 65000 (feet are assumed, <br>
            convert if needed).   </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Altitude:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="altitude_b" value="<?php echo $row_Recordset1['altitude_b']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Altitude:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="altitude_c" value="<?php echo $row_Recordset1['altitude_c']; ?>" size="32"></td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Lens focal length:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="lens_focal_length" value="<?php echo $row_Recordset1['lens_focal_length']; ?>" size="32"><br>
            Example: 12 inches
            (304.8mm). </td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Camera:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="camera" value="<?php echo $row_Recordset1['camera']; ?>" size="32"> <br>
            Example: RC-10, # 76.   </td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Filmtype:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="filmtype" value="<?php echo $row_Recordset1['filmtype']; ?>" size="32"> <br />
Example: Panchromatic or SO-397. </td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Spectral range:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="spectral_range" value="<?php echo $row_Recordset1['spectral_range']; ?>" size="32"><br>
            Example: 510-900 nm. </td>
    </tr>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Filter:</td>
      <td class="MILfont-edit"><input class="MILfont-input-box" type="text" name="filter" value="<?php echo $row_Recordset1['filter']; ?>" size="32"><br>
            Example: Wratten 21. </td>
    </tr>

    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Note:<br>
            Enter all <br>
            supplemental<br>
            information              </td>
      <td><textarea class="MILfont-input-box" name="note" cols="40" id="note"><?php echo $row_Recordset1['note']; ?></textarea></td>
    </tr>

<?php
$row_Recordset1['prod_test']="test";
include("../common_code/include_prod_test.php");
?>
    <tr valign="baseline">
      <td align="right" nowrap class="MILfont-edit">Holding ID:</td>
      <td class="MILfont-edit"><?php echo $row_Recordset1['holding_id']; ?></td>
    </tr>

    </table>

<input type="hidden" name="record_type" value="<?php echo $record_type; ?>">

    <tr colspan="2">
      <td height="60" colspan="2" valign="middle" bgcolor="#FFFFFF"><div align="center">
          <input class="MILfont-edit" name="submit" type="submit" value="Insert AP Flights Catalog Record">
      </div></td>
    </tr>
  </table>

  <input type="hidden" name="MM_insert" value="form1" />
</form>

<?php
include("../common_code/include_staff_footer.php");
?>



</body>
</html>

<?php
mysql_free_result($model);
?>
<script type="text/javascript">
<!--
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
//-->
</script>
