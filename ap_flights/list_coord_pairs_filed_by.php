<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_Recordset1 = 500;
$pageNum_Recordset1 = 0;
if (isset($_GET['pageNum_Recordset1'])) {
  $pageNum_Recordset1 = $_GET['pageNum_Recordset1'];
}
$startRow_Recordset1 = $pageNum_Recordset1 * $maxRows_Recordset1;

$filed_by_pattern = $_POST['filed_by']."%";

$maxRows_Recordset1 = 500;;
$pageNum_Recordset1 = 0;
if (isset($_GET['pageNum_Recordset1'])) {
  $pageNum_Recordset1 = $_GET['pageNum_Recordset1'];
}
$startRow_Recordset1 = $pageNum_Recordset1 * $maxRows_Recordset1;

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = "SELECT * FROM coord_pairs WHERE filed_by like '$filed_by_pattern' ORDER BY filed_by ASC";
$query_limit_Recordset1 = sprintf("%s LIMIT %d, %d", $query_Recordset1, $startRow_Recordset1, $maxRows_Recordset1);
$Recordset1 = mysql_query($query_limit_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);

if (isset($_GET['totalRows_Recordset1'])) {
  $totalRows_Recordset1 = $_GET['totalRows_Recordset1'];
} else {
  $all_Recordset1 = mysql_query($query_Recordset1);
  $totalRows_Recordset1 = mysql_num_rows($all_Recordset1);
}
$totalPages_Recordset1 = ceil($totalRows_Recordset1/$maxRows_Recordset1)-1;

$queryString_Recordset1 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Recordset1") == false &&
        stristr($param, "totalRows_Recordset1") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Recordset1 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_Recordset1 = sprintf("&totalRows_Recordset1=%d%s", $totalRows_Recordset1, $queryString_Recordset1);
?>
<style type="text/css">
<!--
body {
	background-color: #999999;
}
.style3 {font-size: 18px}
.style10 {font-family: Arial, Helvetica, sans-serif; font-size: 10px; }
.style12 {font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; }
-->
</style>
<title>List Coord Pairs for MIL Air Photo Flights</title>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="800"  border="2" align="left" cellpadding="5" cellspacing="5">
      <tr>
        <td width="50%" bgcolor="#FFFFFF"><div align="center" class="style3">
          <div align="left">Coord Pairs for MIL Air Photo Flights</div>
        </div></td>
        <td bgcolor="#FFFFFF"><div align="right">
          <table border="0" width="400" align="left">

          <tr>
                <td width="60%" align="center"><div align="right">Records <?php echo ($startRow_Recordset1 + 1) ?> to <?php echo min($startRow_Recordset1 + $maxRows_Recordset1, $totalRows_Recordset1) ?> of <?php echo $totalRows_Recordset1 ?></div></td>
                <td width="10%" align="center">
                    <div align="left">
                      <?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, 0, $queryString_Recordset1); ?>"><img src="images/First.gif" border=0></a>
                      <?php } // Show if not first page ?>
                  </div></td>
                <td width="10%" align="center">
                    <div align="left">
                      <?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, max(0, $pageNum_Recordset1 - 1), $queryString_Recordset1); ?>"><img src="images/Previous.gif" border=0></a>
                      <?php } // Show if not first page ?>
                  </div></td>
                <td width="10%" align="center">
                    <div align="left">
                      <?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, min($totalPages_Recordset1, $pageNum_Recordset1 + 1), $queryString_Recordset1); ?>"><img src="images/Next.gif" border=0></a>
                      <?php } // Show if not last page ?>
                  </div></td>
                <td width="10%" align="center">
                    <div align="left">
                      <?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, $totalPages_Recordset1, $queryString_Recordset1); ?>"><img src="images/Last.gif" border=0></a>
                      <?php } // Show if not last page ?>
                  </div></td>
              </tr>
          </table>
        </div></td>
      </tr>
      <tr valign="top" class="style10">
        <td bgcolor="#FFFFFF"><div align="left"><a href="insert_coord_pairs.php">Insert new Coord Pairs Record<br>
          </a><a href="list_coord_pairs.php">View all Coord Pairs Records</a> <br>
          <a href="index.php">Return to Main Menu</a></div></td>
        <td bgcolor="#FFFFFF"><form name="form1" method="post" action="list_coord_pairs_filed_by.php">
          <div align="right">View records starting with (filed_by):
              <input name="filed_by" type="text" id="filed_by2">
              <input type="submit" name="Submit" value="Go">
              <br>
              </div>
        </form></td>
      </tr>
    </table>
    <p>&nbsp;    </p></td>
  </tr>
  <tr>
    <td>&nbsp;    </td>
  </tr>
  <tr>
    <td><table border="1" bgcolor="#FFFFFF">
      <tr>
        <td nowrap><span class="style12"><a href="list_coord_pairs.php">holding_id</a><br>
          (select title<br>
          to sort
)        </span></td>
        <td nowrap><span class="style12">filed_by<br>
          (select record <br>
          to modify
)        </span></td>
        <td><span class="style12">polygon<br>
          number</span></td>
        <td><span class="style12">coord1_lat_dir</span></td>
        <td><span class="style12">coord1_lat_deg</span></td>
        <td><span class="style12">coord1_lat_min</span></td>
        <td><span class="style12">coord1_long_dir</span></td>
        <td><span class="style12">coord1_long_deg</span></td>
        <td><span class="style12">coord1_long_min</span></td>
        <td><span class="style12">coord2_lat_dir</span></td>
        <td><span class="style12">coord2_lat_deg</span></td>
        <td><span class="style12">coord2_lat_min</span></td>
        <td><span class="style12">coord2_long_dir</span></td>
        <td><span class="style12">coord2_long_deg</span></td>
        <td><span class="style12">coord2_long_min</span></td>
        <td><span class="style12">coord3_lat_dir</span></td>
        <td><span class="style12">coord3_lat_deg</span></td>
        <td><span class="style12">coord3_lat_min</span></td>
        <td><span class="style12">coord3_long_dir</span></td>
        <td><span class="style12">coord3_long_deg</span></td>
        <td><span class="style12">coord3_long_min</span></td>
        <td><span class="style12">coord4_lat_dir</span></td>
        <td><span class="style12">coord4_lat_deg</span></td>
        <td><span class="style12">coord4_lat_min</span></td>
        <td><span class="style12">coord4_long_dir</span></td>
        <td><span class="style12">coord4_long_deg</span></td>
        <td><span class="style12">coord4_long_min</span></td>
      </tr>
      <?php do { ?>
      <tr>
        <td><span class="style10"><?php echo $row_Recordset1['holding_id']; ?></span></td>
        <td><span class="style10"><a href="update_coord_pairs.php?id=<?php echo $row_Recordset1['id']; ?>"><?php echo $row_Recordset1['filed_by']; ?></a>
          <input name="hiddenField" type="hidden" value="<?php echo $row_Recordset1['id']; ?>">
        </span></td>
        <td><span class="style10"><?php echo $row_Recordset1['polygon_number']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord1_lat_dir']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord1_lat_deg']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord1_lat_min']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord1_long_dir']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord1_long_deg']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord1_long_min']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord2_lat_dir']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord2_lat_deg']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord2_lat_min']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord2_long_dir']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord2_long_deg']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord2_long_min']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord3_lat_dir']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord3_lat_deg']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord3_lat_min']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord3_long_dir']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord3_long_deg']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord3_long_min']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord4_lat_dir']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord4_lat_deg']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord4_lat_min']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord4_long_dir']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord4_long_deg']; ?></span></td>
        <td><span class="style10"><?php echo $row_Recordset1['coord4_long_min']; ?></span></td>
      </tr>
      <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
    </table></td>
  </tr>
  <tr>
    <td><table border="0" width="200" align="left">
      <tr>
        <td colspan="4" align="center"><div align="left">
            <p>&nbsp;</p>
            <p>Records <?php echo ($startRow_Recordset1 + 1) ?> to <?php echo min($startRow_Recordset1 + $maxRows_Recordset1, $totalRows_Recordset1) ?> of <?php echo $totalRows_Recordset1 ?></p>
        </div></td>
      </tr>
      <tr>
        <td width="23%" align="center">
          <div align="left">
            <?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
            <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, 0, $queryString_Recordset1); ?>"><img src="images/First.gif" border=0></a>
            <?php } // Show if not first page ?>
        </div></td>
        <td width="31%" align="center">
          <div align="left">
            <?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
            <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, max(0, $pageNum_Recordset1 - 1), $queryString_Recordset1); ?>"><img src="images/Previous.gif" border=0></a>
            <?php } // Show if not first page ?>
        </div></td>
        <td width="23%" align="center">
          <div align="left">
            <?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
            <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, min($totalPages_Recordset1, $pageNum_Recordset1 + 1), $queryString_Recordset1); ?>"><img src="images/Next.gif" border=0></a>
            <?php } // Show if not last page ?>
        </div></td>
        <td width="23%" align="center">
          <div align="left">
            <?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
            <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, $totalPages_Recordset1, $queryString_Recordset1); ?>"><img src="images/Last.gif" border=0></a>
            <?php } // Show if not last page ?>
        </div></td>
      </tr>
    </table></td>
  </tr>
</table>
<div align="center"></div>
<p align="center">&nbsp;</p>
<?php
mysql_free_result($Recordset1);
?>
