<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

if (isset($_REQUEST['orderby']))  {
$orderby = $_REQUEST['orderby']; }
else {
$orderby = 'filed_by'; }

//*******************************************

if (isset($_REQUEST['sortby']))  {
    $sortby = $_REQUEST['sortby'];
    $order = $_REQUEST['order'];
}
else {
    $sortby = 'filed_by';
    $order = 'ASC';
}
//*  print_r ($_REQUEST);

//*  echo "<br> sortby is " . $sortby . " yyyy and  order is "  . $order . " yeh <br>";
//*  echo "<br> sortby is " . $sortby . " yyyy and  new_order is "  . $new_order_id . " yeh <br>";

$new_order_filed_by = 'ASC';
$new_order_id = 'ASC';


if ($sortby == "filed_by")  {
    if ($order == 'DESC') {
        $new_order_filed_by = 'ASC';
        }
    else {
        $new_order_filed_by = 'DESC';
        }
}
if ($sortby == "id") {
    if ($order == 'DESC') {
        $new_order_id = 'ASC';
        }
    else {
        $new_order_id = 'DESC';
        }
}

//* echo "<br> sortby2 is " . $sortby2 . " yyyy and  order2 is "  . $order2 . " yeh <br>";
//*echo "<br> sortby is " . $sortby . " yyyy and  new_order_begin_date is "  . $new_order_begin_date . " yeh <br>";


//*****************************************************

$maxRows_Recordset1 = 250;
$pageNum_Recordset1 = 0;
if (isset($_GET['pageNum_Recordset1'])) {
  $pageNum_Recordset1 = $_GET['pageNum_Recordset1'];
}
$startRow_Recordset1 = $pageNum_Recordset1 * $maxRows_Recordset1;

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
//* the original$query_Recordset1 = "SELECT * FROM ap_flights where prod_test = 'prod' ORDER BY $orderby ASC";

$query_Recordset1 = "select ap_flights.holding_id as id, filed_by,
filed_by_in_catalog, filed_by_in_collection, begin_date, index_digital,
frames_scanned, ready_ref, prod_test,
    group_concat(distinct country_values.country_id separator '&&') as country_code,
    group_concat(distinct country_values.country separator '&&') as country_name,
    group_concat(state_values.state_code separator '&&')  as state_code,
    group_concat(distinct state_values.state separator '&&') as state_name
from ap_flights
    left outer join ap_flights_loc_country ON ap_flights.holding_id=ap_flights_loc_country.holding_id
    left outer join country_values ON ap_flights_loc_country.country_id=country_values.country_id
    left outer join ap_flights_loc_state ON ap_flights.holding_id=ap_flights_loc_state.holding_id
    left outer join state_values ON ap_flights_loc_state.state_id=state_values.state_id
where prod_test='prod'
group by id
ORDER BY $sortby $order";

$query_limit_Recordset1 = sprintf("%s LIMIT %d, %d", $query_Recordset1, $startRow_Recordset1, $maxRows_Recordset1);
$Recordset1 = mysql_query($query_limit_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);

if (isset($_GET['totalRows_Recordset1'])) {
  $totalRows_Recordset1 = $_GET['totalRows_Recordset1'];
} else {
  $all_Recordset1 = mysql_query($query_Recordset1);
  $totalRows_Recordset1 = mysql_num_rows($all_Recordset1);
}
$totalPages_Recordset1 = ceil($totalRows_Recordset1/$maxRows_Recordset1)-1;

$queryString_Recordset1 = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_Recordset1") == false &&
        stristr($param, "totalRows_Recordset1") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_Recordset1 = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_Recordset1 = sprintf("&totalRows_Recordset1=%d%s", $totalRows_Recordset1, $queryString_Recordset1);
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>List of Production Aerial Photography Catalog Records</title>

<?php
include("../common_code/include_MIL_style_links.php");
?>

<script src="/apcatalog/jquery/jquery.js"></script>

<!-- this is the script that is used for the persist header/persist area classes that are used -->
<!-- to make the static table headers for the long lists of records that are displayed -->
<script type="text/javascript" src="/apcatalog/common_code/include_update_table_headers.js"></script>


</head>

<body class="MILlight-grey MILlink" onResize="window.location=window.location;">

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<table width="100%"  border="0" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td><div align="center" class="MILfont-x-large"><span class="MILfont-bold">Production</span> Aerial Photography Catalog Records<br>
    </td>
  </tr>
</table>

<table class="MILright-margin-10" border="0" cellpadding="0" cellspacing="0"  align="right">
  <tr>
    <td align="right"><?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, 0, $queryString_Recordset1); ?>"><img src="First.gif" alt="Go back to first page" border=0></a>
        <?php } // Show if not first page ?>    <?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, max(0, $pageNum_Recordset1 - 1), $queryString_Recordset1); ?>"><img src="Previous.gif" alt="Go back one page" border=0></a>&nbsp;&nbsp;
        <?php } // Show if not first page ?>    <?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, min($totalPages_Recordset1, $pageNum_Recordset1 + 1), $queryString_Recordset1); ?>"><img src="Next.gif" alt="Go forward 1 page" border=0></a>
        <?php } // Show if not last page ?>    <?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
        <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, $totalPages_Recordset1, $queryString_Recordset1); ?>"><img src="Last.gif" alt="Go to the last page" border=0></a>
    <?php } // Show if not last page ?>    </td>
  </tr>
  <tr>
    <td align="right"><span class="MILfont-list">Records <?php echo ($startRow_Recordset1 + 1) ?> to <?php echo min($startRow_Recordset1 + $maxRows_Recordset1, $totalRows_Recordset1) ?> of <?php echo $totalRows_Recordset1 ?></span> </td>
  </tr>
  <tr>
    <td align="right">
      <form action="list_filed_by.php" method="post" name="form1" class="MILfont-list">
      View records containing (filed_by):
        <input name="filed_by" type="text" id="filed_by2">
    <input type="submit" name="Submit" value="Go">
      </form>
     <td>
  </tr>
</table>

<table width="100%" border="1" class="MILwhite persist-area" cellspacing="0" cellpadding="5">
  <tr class="MILdark-grey MILfont-list persist-header">
    <td class="MILfont-bold" align="center"><a href="list.php?sortby=filed_by&amp;order=<?php echo $new_order_filed_by; ?>">Filed By</a><br>
      <span class="MILfont-x-small">(select record to see<br>
      detailed report)</span></td>
    <td class="MILfont-bold MILline-height-120" align="center">Filed By<br>
    In Catalog</td>
    <td class="MILfont-bold MILline-height-120" align="center">Filed By<br>
    In Collection</td>
    <td class="MILfont-bold MILline-height-120" align="center"><a href="list.php?sortby=id&amp;order=<?php echo $new_order_id; ?>">Holding ID</a></td>
    <td class="MILfont-bold MILline-height-120" align="center">Begin Date</td>
    <td class="MILfont-bold MILline-height-120" align="center">Area</td>
    <td class="MILfont-bold MILline-height-120" align="center">Index<br>Scanned</td>
    <td class="MILfont-bold MILline-height-120" align="center">Frames<br>Scanned</td>
    <td class="MILfont-bold MILline-height-120" align="center">Frequently<br>
      Requested</td>
    <td class="MILfont-bold MILline-height-120" align="center">Edit</td>
  </tr>
  <?php do { ?>
  <tr class="MILfont-list">
    <td class="MILfont-list"><a href="staff_report/report_staff.php?filed_by=<?php echo $row_Recordset1['filed_by']; ?>"><?php echo $row_Recordset1['filed_by']; ?></a></td>
    <td><div align="center" class="MILfont-list"><?php echo $row_Recordset1['filed_by_in_catalog']; ?></div></td>
    <td><div align="center" class="MILfont-list"><?php echo $row_Recordset1['filed_by_in_collection']; ?></div></td>
    <td><div align="center" class="MILfont-list"><?php echo $row_Recordset1['id']; ?></div></td>
    <?php
	// convert mysql date to php timestamp
	$phptimestamp = strtotime( $row_Recordset1['begin_date'] );
	// now format php timestamp
	$begin_date = date( 'Y-m-d ', $phptimestamp );
	?>
    <td nowrap><div align="center" class="MILfont-list"><?php echo $begin_date; ?></div></td>

	<td><div align="center" class="MILfont-list">
	 <?php

	       $countryCode = explode("&&",$row_Recordset1['country_code']);
	       $numCountryCode = count($countryCode);
	       $countryName = explode("&&",$row_Recordset1['country_name']);
	       $stateCode = explode("&&",$row_Recordset1['state_code']);
	       $numStateCode = count($stateCode);
	       $stateName = explode("&&",$row_Recordset1['state_name']);

	       if ($numCountryCode >1) {
	       echo "International";
	       }
	       elseif (($numCountryCode == 1) && ($countryCode[0] != 1))
	       {
	       echo $countryName[0];
	       }
	       elseif ($numStateCode == 1)
	       {
	       echo $stateName[0];
	       }
	       else
	       {
	       echo implode($stateCode, "  ");
	       }
	  ?>
      &nbsp;
	</div></td>

	<?php
	$flight_id_clean = preg_replace('/\s\s+/', '', $row_Recordset1['filed_by']);
    $flight_id_cleaner = preg_replace('/[-._()]/', '', $flight_id_clean);
	$flight_id_lower = strtolower($flight_id_cleaner);
	?>

<?php
include("../common_code/include_report_staff_column_index.php");
?>

	<td>
	<span class="MILfont-list">
	<?php if ($row_Recordset1['frames_scanned'] == 1) {  ?>
	</span>
	<div align="left" class="MILfont-list">Yes
	    <?php }
	else if ($row_Recordset1['frames_scanned'] == 2) {  ?>
	</span>
	<div align="left" class="MILfont-list">Some
	    <?php }
	else { ?>
	  No
	  <?php
	}
	?>
	</div></td>
    <td><span class="MILfont-list">
	<?php if ($row_Recordset1['ready_ref'] == yes) {  ?>
	</span>
	<div align="left" class="MILfont-list"><a href="../ready_ref/list_by_flight.php?holding_id=<?php echo $row_Recordset1['id']; ?>">Yes</a>
	    <?php }
	else { ?>
	  No
	  <?php
	}
	?>
	</div></td>
    <td><div align="center" class="MILfont-list"><a href="update_form.php?record_type=prod&holding_id=<?php echo $row_Recordset1['id']; ?>">Edit</a></div></td>
  </tr>
  <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>

<A NAME="hereNow"></A>

<table  width="100%"  border="0" cellpadding="5" >
<!--  <tr>
  <td colspan="2">&nbsp;</td>
  </tr>
  -->
  <tr>
      <td width="25%" align="left" class="MILfont-list">

&nbsp;
</td>

<!--      <a href="index.php">Return to AP Flights Home</a></td> -->
    <td align="right">

        <table width="100%" border="0" cellpadding="0">
             <tr>
		       <td align="right"><?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
		           <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, 0, $queryString_Recordset1); ?>"><img src="First.gif" alt="Go back to first page" border=0></a>
		           <?php } // Show if not first page ?>    <?php if ($pageNum_Recordset1 > 0) { // Show if not first page ?>
		           <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, max(0, $pageNum_Recordset1 - 1), $queryString_Recordset1); ?>"><img src="Previous.gif" alt="Go back one page" border=0></a>&nbsp;&nbsp;
		           <?php } // Show if not first page ?>    <?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
		           <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, min($totalPages_Recordset1, $pageNum_Recordset1 + 1), $queryString_Recordset1); ?>"><img src="Next.gif" alt="Go forward 1 page" border=0></a>
		           <?php } // Show if not last page ?>    <?php if ($pageNum_Recordset1 < $totalPages_Recordset1) { // Show if not last page ?>
		           <a href="<?php printf("%s?pageNum_Recordset1=%d%s", $currentPage, $totalPages_Recordset1, $queryString_Recordset1); ?>"><img src="Last.gif" alt="Go to the last page" border=0></a>
		       <?php } // Show if not last page ?>    </td>
  </tr>
	       <tr>
	         <td align="right">     <span class="MILfont-list">Records <?php echo ($startRow_Recordset1 + 1) ?> to <?php echo min($startRow_Recordset1 + $maxRows_Recordset1, $totalRows_Recordset1) ?> of <?php echo $totalRows_Recordset1 ?></span>
	         </td>
           </tr>
           <tr>
             <td align="right"><span class="MILfont-list">
    			  <form action="list_filed_by.php" method="post" name="form2" class="MILfont-list">
	     		  View records containing (filed_by):
			      <input name="filed_by" type="text" id="filed_by2">
			      <input type="submit" name="Submit" value="Go">
			      </form>
			      </span>
			 </td>
		   </tr>
        </table>
    </td>
  </tr>

</table>


<?php
include("../common_code/include_staff_footer.php");
?>



</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
