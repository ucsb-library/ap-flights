<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Recordset1 = "-1";
if (isset($_GET['id'])) {
  $colname_Recordset1 = $_GET['id'];
}
mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);

$query_Recordset1 = sprintf("SELECT Ready_Ref.*, ap_flights.holding_id, ap_flights.filed_by FROM Ready_Ref, ap_flights WHERE Ready_Ref.id = %s AND Ready_Ref.holding_id = ap_flights.holding_id", GetSQLValueString($colname_Recordset1, "int"));

$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Delete Record ID <?php echo $row_Recordset1['id']; ?> Confirmation</title>

<?php
include("../common_code/include_MIL_style_links.php");
?>

</head>

<body class="MILlink MILbody-height">

<div id="MILwrap">
   <div id="MILmain">

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<p align="center">&nbsp;</p>

<div align="center">

  <table border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="498" align="center"><h4 align="center" class="MILfont-bold">Are you sure you want to delete this Frequently Requested Flight record?</h4>
       <h4 align="center">Filed By: <span class="MILfont-bold"> <?php echo $row_Recordset1['filed_by']; ?></span></h4>
       <h4 align="center">Holding ID: <span class="MILfont-bold"> <?php echo $row_Recordset1['holding_id']; ?></span></h4>
       <h4 align="center">Record ID: <span class="MILfont-bold"> <?php echo $row_Recordset1['id']; ?></span></h4>
        <br />
        <table width="250" border="1" cellpadding="5" cellspacing="0">
          <tr>

                      <td width="125" class="MILfont-bold"><div align="center"><a href="rr_delete.php?id=<?php echo $row_Recordset1['id']; ?>"> Yes</a></div></td>
                      <td width="125" class="MILfont-bold"><div align="center"><a href="ready_ref_master.php">No</a></div></td>
                    </tr>
                 </table>
              </div></td>
            </tr>
          </table>
        </td>
    </tr>
  </table>
</div>

<p>&nbsp;</p>

   </div> <!-- close of div id="MILmain" -->
</div> <!-- close of div id="MILwrap" -->

<?php
include("../common_code/include_staff_footer_absolute_bottom.php");
?>



</body>
</html>
<?php
mysql_free_result($Recordset1);
?>