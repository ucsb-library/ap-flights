<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_Recordset1 = "SELECT * FROM country_values ORDER BY country_sort_order ASC";
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>List of all Countries</title>

<?php
include("../common_code/include_MIL_style_links.php");
?>

</head>

<body class="MILlight-grey MILlink">

<div id="MILwrap">
   <div id="MILmain">

<?php
include("../common_code/include_staff_header.php");
?>
<br />

<table width="100%"  border="0" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td><div align="center" class="MILfont-x-large">Edit Country Information <br /><br />
    </td>
  </tr>
</table>

<table class="MILwhite MILcenter" border="1" align="center" cellpadding="5"  cellspacing="0">

  <tr class="MILfont-medium">
    <td class="MILfont-bold" align="center">Country</td>
    <td>&nbsp;</td>
  </tr>
  <?php do { ?>
    <tr class="MILfont-list">
      <input name="hiddenField2" type="hidden" id="hiddenField2" value="<?php echo $row_Recordset1['country_sort_order']; ?>
      <input name="hiddenField" type="hidden" id="hiddenField" value="<?php echo $row_Recordset1['country_id']; ?>" />
      <td><?php echo $row_Recordset1['country']; ?>
      </td>
      <td><a href="country_update.php?country_id=<?php echo $row_Recordset1['country_id']; ?>">Edit</a></td>
    </tr>
    <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>

   </div> <!-- close of div id="MILmain" -->
</div> <!-- close of div id="MILwrap" -->



<?php
include("../common_code/include_staff_footer_absolute_bottom.php");
?>



</body>
</html>
<?php
mysql_free_result($Recordset1);
?>
