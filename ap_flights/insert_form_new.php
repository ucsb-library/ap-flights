<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);

$query_Recordset1 = "SELECT * FROM area_general_values ORDER BY area_general ASC";
$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_year = "SELECT * FROM year_lookup ORDER BY `year` DESC";
$year = mysql_query($query_year, $MilWebAppsdb1mysql) or die(mysql_error());
$row_year = mysql_fetch_assoc($year);
$totalRows_year = mysql_num_rows($year);

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_area_general_values = "SELECT * FROM area_general_values ORDER BY area_general ASC";
$area_general_values = mysql_query($query_area_general_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_area_general_values = mysql_fetch_assoc($area_general_values);
$totalRows_area_general_values = mysql_num_rows($area_general_values);

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);
$query_location_values = "SELECT * FROM location_values ORDER BY sort_order ASC";
$location_values = mysql_query($query_location_values, $MilWebAppsdb1mysql) or die(mysql_error());
$row_location_values = mysql_fetch_assoc($location_values);
$totalRows_location_values = mysql_num_rows($location_values);
?>
<style type="text/css">
<!--
.style3 {font-size: large}
.style13 {
	font-size: x-small;
	font-family: Arial, Helvetica, sans-serif;
}
-->
</style>
<title>Insert new AP Flight records</title>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #999999;
}
.style15 {color: #FF0000}
.style16 {font-size: x-small}
.style17 {color: #FF0000; font-size: x-small; }
-->
</style>
<script type="text/javascript">
<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
//-->
</script>
<table width="57%"  border="2" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td bgcolor="#FFFFFF"><div align="center" class="style3">Insert New MIL Air Photo Flights Catalog Record <br />
        <span class="style17">* </span><span class="style16">Indicates required field</span></div></td>
  </tr>
</table>
<form action="insert_mysql.php" method="post" name="form1" onsubmit="MM_validateForm('official_flight_id','','R','filed_by','','R','filed_by_in_catalog','','R','filed_by_in_collection','','R');return document.MM_returnValue">
  <table width="100%" border="2" cellspacing="1" bgcolor="#FFFFFF">
    <tr>
      <td width="50%" valign="top" class="style13"><table width="400"  border="1" align="left" cellpadding="5" bgcolor="#FFFFFF">
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Area General:</td>
          <td colspan="2" align="right" nowrap class="style13">            <div align="left">
            <select name="area_general" id="area_general">
              <option value="none">Please select an area (general):</option>
              <option value="Not listed, see Notes.">Not listed, see Notes.</option>
              <?php
do {
?><option value="<?php echo $row_area_general_values['area_general']?>"><?php echo $row_area_general_values['area_general']?></option>
              <?php
} while ($row_area_general_values = mysql_fetch_assoc($area_general_values));
  $rows = mysql_num_rows($area_general_values);
  if($rows > 0) {
      mysql_data_seek($area_general_values, 0);
	  $row_area_general_values = mysql_fetch_assoc($area_general_values);
  }
?>
            </select>
          </div></td>
          </tr>


        <tr valign="baseline">
          <td align="right" nowrap="nowrap" class="style13">Counties:</td>
          <td colspan="2" class="style13"><input name="counties" type="text" id="counties" size="32" />
              <br />
            Used to search for &quot;Flights by County&quot; when area  = &quot;...Regions&quot;.<br />
            Example: San Diego; Orange; Los Angeles; Ventura; </td>
        </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Ready Ref?</td>
		  <td align="left"class="style13">
			  <input type="radio" name="ready_ref" value="yes" id="ready_ref"/> Yes
			  <input type="radio" name="ready_ref" value="no" id="ready_ref" checked/> No
		  </td>
        </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">&nbsp;</td>
          <td colspan="2" class="style13">&nbsp;</td>
        </tr>
        <tr valign="baseline">
          <td width="27%" align="right" nowrap class="style13">Official <br>
            flight_id:</td>
          <td colspan="2" class="style13"><input name="official_flight_id" type="text" id="official_flight_id" value="" size="32">
            <span class="style15">*</span><br>
            Official full title of flight - first listed on <br>
            catalog record
            (called Flight I.D.)</td>
          </tr>

        <tr valign="baseline">
          <td align="right" nowrap class="style13">Filed_by</td>
          <td colspan="2" class="style13"><input name="filed_by" type="text" id="filed_by" value="" size="32">
            <span class="style15">*</span><br>
            This field should only contain letters,<br>
            numbers and dashes (no other characters). </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Filed_by<br>
            (in_catalog):</td>
          <td colspan="2" class="style13"><input name="filed_by_in_catalog" type="text" id="filed_by_in_catalog" value="" size="32">
            <span class="style15">*</span><br>
            Title flight is filed under in shelflist.</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13"><p>Filed_by<br>
            (in collection):</p>            </td>
          <td colspan="2" class="style13"><input name="filed_by_in_collection" type="text" id="filed_by_in_collection" value="" size="32">
            <span class="style15">*</span><br>
            Title flight is filed under in MIL <br>
            collection
            (usually the same as above, <br>
            except for PAI flights). </td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
          </tr>

        <tr valign="baseline">
          <td align="right" nowrap class="style13">Begin date:</td>
          <td class="style13"><span id="spryselect1">
          <span class="selectRequiredMsg">Please select a year.</br></span>
            <select name="beg_year" id="beg_year">
              <option value="">Year</option>
              <?php
do {
?>
              <option value="<?php echo $row_year['year']?>"><?php echo $row_year['year']?></option>
              <?php
} while ($row_year = mysql_fetch_assoc($year));
  $rows = mysql_num_rows($year);
  if($rows > 0) {
      mysql_data_seek($year, 0);
	  $row_year = mysql_fetch_assoc($year);
  }
?>
            </select>
            </span>
            <select name="beg_month" id="beg_month">
              <option value="01" selected>01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
            </select>
            <select name="beg_day" id="beg_day">
              <option value="01" selected>01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
              <option value="25">25</option>
              <option value="26">26</option>
              <option value="27">27</option>
              <option value="28">28</option>
              <option value="29">29</option>
              <option value="30">30</option>
              <option value="31">31</option>
            </select>
            <span class="style15">*</span></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">End date:</td>
          <td class="style13">              <select name="end_year" id="end_year">
            <option value="none" <?php if (!(strcmp("none", "none"))) {echo "selected=\"selected\"";} ?>Year</option>
            <?php
do {
?><option value="<?php echo $row_year['year']?>"<?php if (!(strcmp($row_year['year'], "none"))) {echo "selected=\"selected\"";} ?>><?php echo $row_year['year']?></option><?php
} while ($row_year = mysql_fetch_assoc($year));
  $rows = mysql_num_rows($year);
  if($rows > 0) {
      mysql_data_seek($year, 0);
	  $row_year = mysql_fetch_assoc($year);
  }
?>
          </select>
            <select name="end_month" id="end_month">
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12" selected>12</option>
              </select>
            <select name="end_day" id="end_day">
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
              <option value="13">13</option>
              <option value="14">14</option>
              <option value="15">15</option>
              <option value="16">16</option>
              <option value="17">17</option>
              <option value="18">18</option>
              <option value="19">19</option>
              <option value="20">20</option>
              <option value="21">21</option>
              <option value="22">22</option>
              <option value="23">23</option>
              <option value="24">24</option>
              <option value="25">25</option>
              <option value="26">26</option>
              <option value="27">27</option>
              <option value="28">28</option>
              <option value="29">29</option>
              <option value="30">30</option>
              <option value="31" selected>31</option>
                        </select>
            <br>
              End date is not required if <br>
              flight
            has only one date. </td></tr>
        <tr class="style13">
          <td colspan="2">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">First scale:</td>
          <td class="style13"><input type="text" name="scale_1" value="" size="32">
            <br>
            Just enter number. Example: 12000 </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Second scale:</td>
          <td class="style13"><input type="text" name="scale_2" value="" size="32">            </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Third scale:</td>
          <td class="style13"><input type="text" name="scale_3" value="" size="32">            </td>
          </tr>

       <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Index type:</td>
          <td colspan="2" class="style13">
           <select name="index_type[]" id="index_type" multiple>
            <option value="none" selected="selected">Please select an index type:</option>
            <option value="mosaic">mosaic</option>
            <option value="line">line</option>
            <option value="spot">spot</option>
			<option value="spot">self-indexed</option>
			<option value="none">none</option>
			<option value="Not listed, see Notes.">Not listed, see Notes</option>
            </select></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Index scale:</td>
          <td colspan="2" class="style13"><input type="text" name="index_scale" value="" size="32">
            <br>
             Example: 12000, 24000, 50000 </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Index filed under:</td>
          <td colspan="2" class="style13"><input type="text" name="index_filed_under" value="" size="32">
            <br>
            If index filed under alternative name, <br>
            list that name here.</td>
          </tr>

<tr valign="baseline">
          <td align="right" nowrap class="style13">Location:</td>
		  <td td colspan="2" class="style13">
<?php
  do {
?>
<input type ="checkbox" name="chkLocation[]" value="<?php echo $row_location_values['id']?>"><?php echo $row_location_values['location']?>
              <?php
} while ($row_location_values = mysql_fetch_assoc($location_values));
  $rows = mysql_num_rows($location_values);
  if($rows > 0) {
      mysql_data_seek($location_values, 0);
	  $row_location_values = mysql_fetch_assoc($location_values);
  }
?>
 <!--           </select> -->
          </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Imagery location:</td>
          <td colspan="2" class="style13"><input type="text" name="special_location" value="" size="32">
            <br>
            Enter if imagery filed in unusual location: <br>
            Example: Roll, Film, Collection  </td>
          </tr>
         <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Size:</td>
          <td colspan="2" class="style13"><input type="text" name="size" value="" size="32">
            <br>
            Example: frames 9 x 9 inches or frames 70mm </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Height:</td>
          <td colspan="2" class="style13"><input type="text" name="height" value="" size="32">
            <br>
            List height in inches.</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Width:</td>
          <td colspan="2" class="style13"><input type="text" name="width" value="" size="32">
            <br>
List width in inches.</td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
          </tr>

      <tr valign="baseline">
          <td align="right" nowrap class="style13">Overlap:</td>
          <td class="style13"><input type="text" name="overlap" value="" size="32">
            <br>
            Enter % overlap. Example:  60%. </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Sidelap:</td>
          <td class="style13"><input type="text" name="sidelap" value="" size="32">
            <br>
            Enter % sidelap. Example:  20%. </td>
          </tr>
        <tr class="style13">
          <td colspan="2">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Spectral range:</td>
          <td class="style13"><input type="text" name="spectral_range" value="" size="32">
            <br>
            Example: 510-900 nm. </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Filter:</td>
          <td class="style13"><input type="text" name="filter" value="" size="32">
            <br>
            Example: Wratten 21. </td>
          </tr>
        <tr class="style13">
          <td colspan="2">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Generation held:</td>
          <td class="style13"><input type="text" name="generation_held" value="" size="32">
            <br>
            Example: 3rd generation or <br>            1st and 2nd generation. </td>
          </tr>
 
      </table></td>
      <td width="50%" valign="top" class="style13"><table width="400"  border="1" cellpadding="5" bgcolor="#FFFFFF">

	
<?php
include("../common_code/include_physical_fields_details_display.php");
?>

<!-- ***********        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" valign="top" nowrap>
            <div align="left">
              <table width="40%"  border="0" align="left">
                  <tr>
                    <td colspan="3" class="style13"><strong>Physical Details:</strong></td>
                  </tr>
                  <tr>
                    <td class="style13">&nbsp;</td>
                    <td class="style13"><div align="center">yes</div></td>
                    <td class="style13"><div align="center">no</div></td>
                  </tr>
                  <tr>
                    <td class="style13"><div align="right">Bw::</div></td>
                    <td class="style13"><div align="center">
                      <input name="bw" type="radio" value="1" />
                    </div></td>
                    <td class="style13"><div align="center">
                      <input name="bw" type="radio" value="0" checked="checked" />
                    </div></td>
                  </tr>
                  <tr>
                    <td class="style13"><div align="right">Bw IR::</div></td>
                    <td class="style13"><div align="center">
                      <input name="bw_IR" type="radio" value="1" />
                    </div></td>
                    <td class="style13"><div align="center">
                      <input name="bw_IR" type="radio" value="0" checked="checked" />
                    </div></td>
                  </tr>
                  <tr>
                    <td class="style13"><div align="right">Color:</div></td>
                    <td class="style13"><div align="center">
                      <input name="color" type="radio" value="1" />
                    </div></td>
                    <td class="style13"><div align="center">
                      <input name="color" type="radio" value="0" checked="checked" />
                    </div></td>
                  </tr>
                  <tr>
                    <td class="style13"><div align="right">Color IR:: </div></td>
                    <td class="style13"><div align="center">
                      <input name="color_IR" type="radio" value="1" />
                    </div></td>
                    <td class="style13"><div align="center">
                      <input name="color_IR" type="radio" value="0" checked="checked" />
                    </div></td>
                  </tr>
                  <td class="style13"><div align="right">Print::</div></td>
                  <td class="style13"><div align="center">
                    <input name="printt" type="radio" value="1" />
                    </div></td>
                  <td class="style13"><div align="center">
                    <input name="printt" type="radio" value="0" checked="checked" />
                    </div></td>
                </tr>
              <tr>
                <td class="style13"><div align="right">Pos trans::</div></td>
                  <td class="style13"><div align="center">
                    <input name="pos_trans" type="radio" value="1" />
                    </div></td>
                  <td class="style13"><div align="center">
                    <input name="pos_trans" type="radio" value="0" checked="checked" />
                    </div></td>
                </tr>
              <tr>
                <td class="style13"><div align="right">Negative::</div></td>
                  <td class="style13"><div align="center">
                    <input name="negative" type="radio" value="1" />
                    </div></td>
                  <td class="style13"><div align="center">
                    <input name="negative" type="radio" value="0" checked="checked" />
                    </div></td>
                </tr>
              <tr>
                <td class="style13"><div align="right">Digital:: </div></td>
                  <td class="style13"><div align="center">
                    <input name="digital" type="radio" value="1" />
                    </div></td>
                  <td class="style13"><div align="center">
                    <input name="digital" type="radio" value="0" checked="checked" />
                    </div></td>
                </tr>
              <tr>
                <td class="style13"><div align="right">Roll:</div></td>
                  <td class="style13"><div align="center">
                    <input name="roll" type="radio" value="1" />
                    </div></td>
                  <td class="style13"><div align="center">
                    <input name="roll" type="radio" value="0" checked="checked" />
                    </div></td>
                </tr>
              <tr>
              <td class="style13"><div align="right">Cut frame:</div></td>
              <td class="style13"><div align="center">
                <input name="cut_frame" type="radio" value="1" />
              </div></td>
              <td class="style13"><div align="center">
                <input name="cut_frame" type="radio" value="0" checked="checked" />
              </div></td>
            </tr><tr>
              <td class="style13"><div align="right">Vertical:</div></td>
              <td class="style13"><div align="center">
                <input name="vertical" type="radio" value="1" />
              </div></td>
              <td class="style13"><div align="center">
                <input name="vertical" type="radio" value="0" checked="checked" />
              </div></td>
            </tr>
            <tr>
              <td class="style13"><div align="right">Oblique high:</div></td>
              <td class="style13"><div align="center">
                <input name="oblique_high" type="radio" value="1" />
              </div></td>
              <td class="style13"><div align="center">
                <input name="oblique_high" type="radio" value="0" checked="checked" />
              </div></td>
            </tr>
            <tr>
              <td class="style13"><div align="right">Oblique low: </div></td>
              <td class="style13"><div align="center">
                <input name="oblique_low" type="radio" value="1" />
              </div></td>
              <td class="style13"><div align="center">
                <input name="oblique_low" type="radio" value="0" checked="checked" />
              </div></td>
            </tr>
                </table>
            </div></td>
          </tr>
 ****** -->
 
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Filmtype:</td>
          <td class="style13"><input type="text" name="filmtype" value="" size="32" />
            <br />
Example: Panchromatic or SO-397. </td>
        </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Camera:</td>
          <td class="style13"><input type="text" name="camera" value="" size="32">
            <br>
            Example: RC-10, # 76.   </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Lens focal length:</td>
          <td class="style13"><input type="text" name="lens_focal_length" value="" size="32">
            <br>
            Example: 12 inches
            (304.8mm). </td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="2" align="right" nowrap>&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Platform id:</td>
          <td class="style13"><input type="text" name="platform_id" value="" size="32">
            <br>
            Example: U-2 Aircraft #5 .              </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Directional <br>
            orientation:</td>
          <td class="style13"><input type="text" name="directional_orientation" value="" size="32">
            <br>
            Example: West-East; Northwest-Southeast             </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">&nbsp;</td>
          <td class="style13">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Altitude a:</td>
          <td class="style13"><input type="text" name="altitude_a" value="" size="32">
            <br>
            Example: 65000 (feet are assumed, <br>
            convert if needed).   </td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Altitude b:</td>
          <td class="style13"><input type="text" name="altitude_b" value="" size="32"></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Altitude c:</td>
          <td class="style13"><input type="text" name="altitude_c" value="" size="32"></td>
          </tr>
        <tr class="style13">
          <td colspan="2">&nbsp;</td>
          </tr>
		  
       <tr valign="baseline">
          <td align="right" nowrap class="style13">Index digital:</td>
          <td colspan="2" class="style13">
          <table width="100%" border="0">
            <tr align="left" valign="middle">
              <td width="5%" class="style13">
                <input name="index_digital" type="radio" value="1" />Yes</td>
              <td width="5%" class="style13">
                <input name="index_digital" type="radio" value="0" checked/>No</td>
              <td width="90%" class="style13">
                <input name="index_digital" type="radio" value="2" />Needs rescanning</td>
            </tr>
          </table>
          </td>
        </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Frames scanned:</td>
          <td colspan="2" class="style13">
          <table width="100%" border="0">
            <tr align="left" valign="middle">
              <td width="20%" class="style13">
                <input name="frames_scanned" type="radio" value="1" />Yes</td>
              <td width="20%" class="style13">
                <input name="frames_scanned" type="radio" value="2" />Some</td>
              <td width="20%" class="style13">
                <input name="frames_scanned" type="radio" value="0" checked/>No</td>
              <td width="40%" class="style13">   &nbsp; </td>
              </td>
              <tr>
              <td width="100%" colspan="4" align="left" class="style13">
                <input name="frames_scanned" type="radio" value="3" /> Needs rescanning</td>
            </tr>
          </table>
          </td>
        </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Estimated <br>
            frame count:</td>
          <td colspan="2" class="style13"><input type="text" name="estimated_frame_count" value="" size="32">
              <br>
    Number of frames held <br>
    (put in estimate if exact # not known). </td>
        </tr>	  

        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
        </tr>
        <tr valign="baseline">
          <td colspan="3" align="left" nowrap class="style13"><select name="copyright" id="copyright">
            <option value="none" selected="selected">Please select copyright information:</option>
            <option value="Reproduction rights held by the Regents of the University of California.">Reproduction rights held by the UC Regents.</option>
            <option value="Copyright &copy;  UC Regents.  All Rights Reserved.">Copyright &copy; UC Regents. All Rights Reserved.</option>
            <option value="Copyright &copy;  Pacific Western Aerial Surveys">Copyright &copy; Pacific Western Aerial Surveys</option>
            <option value="Copyright &copy;  I.K. Curtis">Copyright &copy; I.K. Curtis</option>
            <option value="Copyright &copy;  Air Photo USA">Copyright &copy; Air Photo USA</option>
            <option value="Copyright &copy; WAC Corporation">Copyright &copy; WAC Corporation</option>
            <option value="Copyright &copy;  Hong Kong Government">Copyright &copy; Hong Kong Government</option>
            <option value="Copyright &copy;  Real Estate Data Inc.">Copyright &copy; Real Estate Data Inc.</option>
            <option value="Copyright &copy; Bud Kimball Photography">Copyright &copy; Bud Kimball Photography</option>
            <option value="Copyright &copy; Rupp Aerial Photography">Copyright &copy; Rupp Aerial Photography</option>
            <option value="Copyright &copy; California Real Estate and Zoning Aerial Survey">Copyright &copy; California Real Estate and Zoning Aerial Survey</option>
            <option value="Not listed, see Notes.">Not listed, see Notes.</option>
          </select></td>
          </tr>
        <tr valign="baseline">
          <td colspan="3" align="right" nowrap class="style13"><div align="left">
            <select name="access_limitations" id="access_limitations">
              <option value="none">Please select access limitations:</option>
              <option value="none">None</option>
              <option value="UC only">UC only</option>
              <option value="See Staff">See Staff</option>
              <option value="Not listed">Not listed, see Notes.</option>
            </select>
          </div></td>
          </tr>
        <tr valign="baseline" class="style13">
          <td colspan="3" align="right" nowrap>&nbsp;</td>
          </tr>
 <!--  *******************************************
      </table></td>
      <td width="50%" valign="top" class="style13"><table width="400"  border="1" cellpadding="5" bgcolor="#FFFFFF">
********************************** -->
        <tr valign="baseline">
          <td align="right" nowrap class="style13">&nbsp;</td>
          <td class="style13">&nbsp;</td>
          </tr>
  
         <tr valign="baseline">
          <td align="right" nowrap class="style13">Contractor <br>
            requestor:</td>
          <td class="style13"><input type="text" name="contractor_requestor" value="" size="32"></td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Flown by:</td>
          <td class="style13"><input type="text" name="flown_by" value="" size="32">
            <br>
            Example: U.S. Geological Survey</td>
          </tr>
        <tr valign="baseline">
          <td align="right" nowrap class="style13">Acquired from:</td>
          <td class="style13"><input type="text" name="acquired_from" value="" size="32">
            <br>
            Example: U.S. Forest Service or <br>            Landiscor Aerial Information.             </td>
          </tr>
        <tr class="style13">
          <td colspan="2">&nbsp;</td>
          </tr>
        <tr valign="baseline">
          <td align="right" valign="top" nowrap class="style13">Note:<br>
            Enter all <br>
            supplemental<br>
            information              </td>
          <td class="style13">              <textarea name="note" cols="30" rows="12" id="note"></textarea>             </td></tr>
      </table></td>
    </tr>
    <tr class="style13">
      <td height="105" colspan="2" valign="top" bgcolor="#FFFFFF"><div align="center">
        <p>&nbsp;          </p>
        <p>
          <input name="submit" type="submit" value="Insert AP Flights Catalog Record">
</p>
        <p>&nbsp;            </p>
      </div></td>
    </tr>
  </table>
</form>

<?php
mysql_free_result($Recordset1);

mysql_free_result($year);

mysql_free_result($area_general_values);

mysql_free_result($location_values);
?>
<script type="text/javascript">
<!--
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
//-->
</script>
