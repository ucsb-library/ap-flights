<div id="MILskip-links" title="Links that allow you to skip to certain parts of the page">
    <a href="#MILcontent-wrapper">Skip to main content</a>
    <a href="#MILmain-menu">Skip to navigation</a>
</div>

<div id="MILheader">
	<div id="MILucsb-logo-wrapper">
		<a id="MILucsb-logo" title="UCSB Library Home Page" href="http://www.library.ucsb.edu"><span>UCSB Library Home Page</span></a>
	</div>
</div>

<div id="MILmain-menu-wrapper" class="MILmain-menu-bottom-shade MILfont-bold">
	<ul id="MILmain-menu">
		<li class="first"><a href="/apcatalog/">Aerial Photography Information</a></li>
		<li><a href="https://www.library.ucsb.edu/geospatial">Geospatial Collection</a></li>
		<li class="last"><a href="https://www.library.ucsb.edu/geospatial/contact-us">Contact Us</a></li>
	</ul>
</div>
