<?php

//* ALL the style links for all MIL pages to match the current lemon Drupil site
//* M Rankin - March 2012

?>

<link rel="shortcut icon" href="/apcatalog/common_styles/sites/default/files/pingv_ucsb_favicon.ico" type="image/x-icon" />
<meta http-equiv="X-UA-Compatible" value="IE=9" /><meta http-equiv="X-UA-Compatible" value="IE=8"> <meta http-equiv="X-UA-Compatible" value="IE=7">

<!--
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/themes/ninesixty/styles/framework/reset.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/themes/ninesixty/styles/framework/text.css" />
-->

<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/themes/ninesixty/styles/framework/960.css" />

<!--
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/themes/ninesixty/styles/framework/debug.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/modules/node/node.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/modules/system/defaults.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/modules/system/system.css" />
-->

<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/modules/system/system-menus.css" />

<!--
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/modules/user/user.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/cck/theme/content-module.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/ctools/css/ctools.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/date/date.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/date/date_popup/themes/datepicker.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/date/date_popup/themes/jquery.timeentry.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/filefield/filefield.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/megamenu/megamenu.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/megamenu/megamenu-skins.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/mollom/mollom.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/views_slideshow/contrib/views_slideshow_singleframe/views_slideshow.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/misc/farbtastic/farbtastic.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/calendar/calendar.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/cck/modules/fieldgroup/fieldgroup.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/modules/contrib/views/css/views.css" />
<link type="text/css" rel="stylesheet" media="all" href="/apcatalog/common_styles/sites/all/themes/ninesixty/styles/styles.css" />


<link type="text/css" rel="stylesheet" media="screen" href="/apcatalog/common_styles/sites/all/themes/pingv_ucsb/styles/reset.css" />
-->
<link type="text/css" rel="stylesheet" media="screen" href="/apcatalog/common_styles/sites/all/themes/pingv_ucsb/styles/type.css" />
<link type="text/css" rel="stylesheet" media="screen" href="/apcatalog/common_styles/sites/all/themes/pingv_ucsb/styles/layout.css" />
<link type="text/css" rel="stylesheet" media="screen" href="/apcatalog/common_styles/sites/all/themes/pingv_ucsb/styles/colors.css" />
<link type="text/css" rel="stylesheet" media="screen" href="/apcatalog/common_styles/sites/all/themes/pingv_ucsb/styles/styles.css" />

<!--
<link type="text/css" rel="stylesheet" media="screen" href="/apcatalog/common_styles/sites/all/themes/pingv_ucsb/styles/forms.css" />
<link type="text/css" rel="stylesheet" media="screen" href="/apcatalog/common_styles/sites/all/themes/pingv_ucsb/styles/megamenu-custom.css" />
-->

<link type="text/css" rel="stylesheet" href="/apcatalog/common_styles/MILstyles.css" >