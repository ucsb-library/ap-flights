<?php

//*compose display headers for ap_indexes reports
//*
//* the header infpormation is unique to the type of flight that the patron
//* has requested to see
?>

<div class="MILleft-margin-40 MILright-margin-40">

<?php
if (($report_type == 'AllFlights') and ($report_frames_scanned_type == '99')) {
?>
<table  align="left" width="100%" class="MILtop-margin-10" border="0" cellspacing="0" cellpadding="0">
<?php
}
else
{
?>
<table  align="left"  width="100%" class="MILtop-margin-10" border="0" cellspacing="0" cellpadding="5">
<?php
}
?>

  <tr>
    <td valign="top"><p>

  <?php
    if ($report_type == 'county') {
  ?>

    <h1>All Flights for <span class="MILfont-blue MILfont-bold"><?php echo $row_Recordset1['county']; ?></span> County </h1><ul>

  <?php
    }
    else if ($report_type == 'AllCa') {
  ?>

   <h1>All California Flights</h1><ul>
   <li class="MILfont-medium">includes those flights that cross state lines</li>

  <?php
    }
    else if ($report_type == 'AllNonCa') {
  ?>

   <h1>All other (non-California) Flights</h1><ul>
   <li class="MILfont-medium"> excludes flights that are contained completely within the border of California
   </li>

  <?php
    }
    else if ($report_type == 'AllUS') {
  ?>

   <h1>All US Flights</h1><ul>

   <?php
    }
    else if ($report_type == 'AllNonUS') {
  ?>

   <h1>Foreign Flights</h1><ul>
   <li class="MILfont-medium">all foreign flights including those that cross the US borders</li>

  <?php
    }
    else if (($report_type == 'AllFlights') and ($report_frames_scanned_type == '1')) {
  ?>

   <h1>All Digital Flights</h1><ul>
   <li class="MILfont-medium MILbottom-margin-point5em">flights that were born digital, acquired in digital format only, or for which all of the MIL's photos have been scanned</li>

  <?php
      }
      else if (($report_type == 'AllFlights') and ($report_frames_scanned_type == '2')) {
    ?>

     <h1>All Partially Digital Flights</h1><ul>
     <li class="MILfont-medium MILbottom-margin-point5em">flights for which MIL has some digital and some analog holdings</li>

  <?php
      }
      else if (($report_type == 'AllFlights') and ($report_frames_scanned_type == '0')) {
    ?>

    <h1>Analog Only Flights</h1><ul>
     <li class="MILfont-medium MILbottom-margin-point5em">MIL has only film or prints of these flights</li>

  <?php
    }
    else if (($report_type == 'AllFlights') and ($report_frames_scanned_type == '99')) {
  ?>

   <h1>All Cataloged Flights</h1><ul>
   <li class="MILfont-medium">in the UCSB Map & Imagery Laboratory Air Photo Flight collection</div>
   <li class="MILfont-medium">MIL continues to add flights to its archive and has a backlog of uncataloged flights. If you cannot find what you are
looking for, please ask.</li>
  <?php
    }

	if ($report_type == 'AllNonUS') {

    ?>
    <li class="MILfont-medium ">
    <?php
    echo  $totalRows_Recordset1 . " flights in " . $totalRows_RecordsetNumCountries . " countries ";
    }
    else {
    ?>
    <li class="MILfont-medium MILbottom-margin-point8em">
    <?php
    echo $totalRows_Recordset1 . " flights found ";
    }

 if ($report_type == 'county') {
  ?>
      </li></ul><p ><span class="MILfont-medium MILlink"><a href="/apcatalog/california-counties.php">Return to list of counties</a></span></p>
   <?php
 }
 ?>

 </td>

 <?php  if ($report_type == 'county') {
  ?>
      <td align="right" class="MILtop-padding-10"><img src="../images/images_counties/<?php
      if(empty($row_Recordset1['county_map_name'])){
        echo str_replace(' ', '', $row_Recordset1['county']).".jpg";
      }else{
      echo $row_Recordset1['county_map_name'];
      }
      ?>" alt=" <?php echo $row_Recordset1['county']; ?> County" height="294" width="294" />
      </td>
   <?php
 }
 ?>


  </tr>
</table>

</div> <!-- close of div  class="MILleft-margin-40"> -->