<div id="MILfooter2" >
<div id="footer" class="container-12 clear-block">
   <div id="supernav"></div>
      <div id="MILfooter" class="grid-12">
         <div id="block-menu-menu-navigation-footer-primary" class="block block-menu grid-7">
            <div class="content">
			   <ul class="menu">
			      <li class="leaf last"><a href="https://www.library.ucsb.edu" title="Library Home">Library Home</a></li>
		       </ul>
		    </div>
         </div>
         <div id="block-block-17" class="block block-block grid-7">
            <div class="content">
               <p>Copyright &copy; 2012 The Regents of the University of California, All Rights Reserved.</p>
            </div>
         </div>
         </div>
      </div>
   </div>
</div>
</div>