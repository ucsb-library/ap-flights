<?php require_once('../Connections/MilWebAppsdb1mysql.php'); ?>
<?php
if (isset($_REQUEST['sortby']))  {
$sortby = $_REQUEST['sortby'];
$order = $_REQUEST['order'];
}
else {
$sortby = 'begin_date';
$order = ASC;
}


if (isset($_REQUEST['sortby2']))  {
$sortby2 = $_REQUEST['sortby2'];
$sortby2 = ', '.$sortby2;
$order2 = $_REQUEST['order2'];
}
else {
$sortby2 = '';
$order2 = '';
}

$planet_id = '1';

if (isset($_REQUEST['report_type']))  {
$report_type = $_REQUEST['report_type'];
}
else {
$report_type = 'CAFlights';
}

$state_id = "5";
$country_id="1";

if (isset($_GET['state_id'])) {
  $state_id = $_GET['state_id'];
}
if (isset($_GET['country_id'])) {
  $country_id = $_GET['country_id'];
}



if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Recordset1 = "VENTURA";
if (isset($_GET['County'])) {
  $colname_Recordset1 = $_GET['County'];
}

mysql_select_db($database_MilWebAppsdb1mysql, $MilWebAppsdb1mysql);

if ($report_type == 'AllNonCa') {
	$query_Recordset1 = sprintf("SELECT distinct ap_flights.holding_id,
	ap_flights_loc_state.state_id, ap_flights_loc_state.country_id, state_values.state,
	ap_flights.filed_by,ap_flights.begin_date,
	ap_flights.scale_1, ap_flights.index_digital, ap_flights.frames_scanned
		FROM ap_flights, ap_flights_loc_country, ap_flights_loc_state, state_values
		WHERE ap_flights.holding_id=ap_flights_loc_state.holding_id
		and ap_flights_loc_state.state_id != '5'
		and ap_flights_loc_state.country_id = '1'
	  and ap_flights_loc_state.state_id = state_values.state_id
		and ap_flights.prod_test = 'prod' order by ap_flights_loc_state.state_id, ap_flights.begin_date desc");
}

if ($report_type == 'AllNonUS') {
	$query_Recordset1 = "SELECT distinct ap_flights.holding_id, country_values.country, ap_flights.official_flight_id, ap_flights.official_flight_id, ap_flights.filed_by,ap_flights.filed_by_in_catalog, ap_flights.index_digital, ap_flights.begin_date, ap_flights.scale_1,ap_flights.note, ap_flights.copyright,  ap_flights.bw,color, ap_flights.bw_IR, ap_flights.color_IR,ap_flights.printt, ap_flights.pos_trans, ap_flights.negative, ap_flights.digital, ap_flights.roll,ap_flights.vertical,ap_flights.oblique_high, ap_flights.oblique_low, ap_flights.cut_frame, ap_flights.digital,ap_flights.roll,ap_flights.vertical, ap_flights.oblique_high,ap_flights.oblique_low, ap_flights.cut_frame,
	ap_flights_loc_country.country_id
	FROM ap_flights, country_values, ap_flights_loc_country
	WHERE ap_flights.holding_id=ap_flights_loc_country.holding_id
	and ap_flights_loc_country.country_id = country_values.country_id
	and ap_flights_loc_country.country_id != '1'
	and ap_flights.prod_test = 'prod'
	order by country_values.country, ap_flights.begin_date desc";

	$query_RecordsetNumCountries = "SELECT 	distinct ap_flights_loc_country.country_id
		FROM ap_flights, country_values, ap_flights_loc_country
		WHERE ap_flights.holding_id=ap_flights_loc_country.holding_id
		and ap_flights_loc_country.country_id != '1'
	and ap_flights.prod_test = 'prod'";

$RecordsetNumCountries = mysql_query($query_RecordsetNumCountries, $MilWebAppsdb1mysql) or die(mysql_error());
$row_RecordsetNumCountries = mysql_fetch_assoc($RecordsetNumCountries);
$totalRows_RecordsetNumCountries = mysql_num_rows($RecordsetNumCountries);

}

$Recordset1 = mysql_query($query_Recordset1, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset1 = mysql_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysql_num_rows($Recordset1);

$holding_id = $row_Recordset1['holding_id'];

$selected_county = strtolower($colname_Recordset1);

$query_Recordset2 = sprintf("SELECT county_values.county_id, county_values.state_id from county_values where county_values.county=%s",GetSQLValueString($selected_county,"text"));
$Recordset2 = mysql_query($query_Recordset2, $MilWebAppsdb1mysql) or die(mysql_error());
$row_Recordset2 = mysql_fetch_assoc($Recordset2);
$totalRows_Recordset2 = mysql_num_rows($Recordset2);

//* in for diagnostic purposes
//* echo "<br> total number of records selected for report type " . $report_type . " is " . $totalRows_Recordset1 . " yeh!!!! <br>" ;
//* echo "<br> report sort key " . $resortkey . " is " . $resortkey .  " yeh!!!!123 <br>" ;

?>
<?php  $lastTFM_nest = "";?>


<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<!-- Note: the above conditional statements allow the use of ie version specific selectors in stylesheet. This is a better workaround than using CSS Hacks - mirie 2011 11 22; added in language for ADA requirements - mrankin 07-11-2012 usage pioneered by Paul Irish -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php
include("../common_code/include_MIL_all_style_links.php");
?>

<?php
include("../common_code/include_ap_indexes_title_header.php");
?>

<script src="/apcatalog/jquery/jquery.js"></script>

<style type="text/css">
<!--
.table { empty-cells:show; }
-->
</style>

<!-- this is the script that is used for the persist header/persist area classes that are used -->
<!-- to make the static table headers for the long lists of records that are displayed -->
<script type="text/javascript" src="/apcatalog/common_code/include_update_table_headers.js"></script>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/apcatalog/common_code/include_ga.php"); ?>
</head>

<body onResize="window.location=window.location;">

<?php
include("../common_code/include_MIL_header.php");
?>

<?php
include("../common_code/include_ap_indexes_report_header.php");
?>

	<?php
	     $first_region_display = true; // special processing required for top of the list display
	?>

<div class="MILleft-margin-40">

  <?php do {

//*echo "<br> i am here <br>";
//*echo "<br> report_type is xxxx " . $report_type . " yyyy <br>";
//*echo "<br> row_Recordset1['state_id'] is xxxx " . $row_Recordset1['state_id'] . " yyyy <br>";
//*echo "<br> row_Recordset1['country_id'] is xxxx " . $row_Recordset1['country_id'] . " yyyy <br>";


    if ($report_type == 'AllNonCa') {
        $TFM_nest = $row_Recordset1['state_id'];
        }
//*        else if ($report_type == 'AllNonUS')
        else {
        $TFM_nest = $row_Recordset1['country_id'];
        }

//*    $TFM_nest = $row_Recordset1['state_id'];
    $WTF_nest = $planet_id;
if (($lastTFM_nest != $TFM_nest) or ($TFM_nest == NULL)) { //beginning of nested loop here
	$lastTFM_nest = $TFM_nest;

	if (!$first_region_display) {
		?>
    </table>
    <?php
    }
	?>

<TABLE class="persist-area" width="90%" BORDER=0 align="left" cellspacing=0 CELLPADDING=5 >

    <tr>
      <td class="MILfont-bold" colspan="5" align="left"><br />
            <?php
                if ($report_type == 'AllNonCa') {
			        echo $row_Recordset1['state'];
			        }
			        else if ($report_type == 'AllNonUS') {
			        echo $row_Recordset1['country'];
			        }
             ?>
      </td>
    </tr>

    <TR class="MILmid-grey persist-header">
      <td width="10%" class="colHeader MILlink MILfont-medium" nowrap="nowrap"> <a href="county_new_sort.php?sortby=begin_date&amp;order=ASC&amp;report_type=<?php echo $report_type; ?>&amp;state_id=<?php echo $row_Recordset1['state_id']; ?>&amp;country_id=<?php echo $row_Recordset1['country_id']; ?>">Begin Date</a></td>

    <td class="colHeader MILlink MILfont-medium" width="35%" class="colHeader MILlink MILfont-medium"> <a href="county_new_sort.php?sortby=filed_by&amp;order=ASC&amp;report_type=<?php echo $report_type; ?>&amp;state_id=<?php echo $row_Recordset1['state_id']; ?>&amp;country_id=<?php echo $row_Recordset1['country_id']; ?>">Flight ID</a><br />
      <span valign="bottom" class="MILline-height-150 MILfont-x-small">View flight record</span></td>

    <td width="10%" class="colHeader MILlink MILfont-medium"> <a href="county_new_sort.php?sortby=scale_1&amp;order=ASC&amp;report_type=<?php echo $report_type; ?>&amp;state_id=<?php echo $row_Recordset1['state_id']; ?>&amp;country_id=<?php echo $row_Recordset1['country_id']; ?>">Scale</a></td>

    <td width="15%" class="colHeader MILlink MILfont-medium"> Index</td>

    <td width="30%" class="colHeader MILlink MILfont-medium"> <a href="county_new_sort.php?sortby=frames_scanned&amp;order=DESC&amp;sortby2=begin_date&amp;order2=ASC&amp;report_type=<?php echo $report_type; ?>&amp;state_id=<?php echo $row_Recordset1['state_id']; ?>&amp;country_id=<?php echo $row_Recordset1['country_id']; ?>">Digital Frames Available</a></td>
    </tr>

    <?php } //End of Nested Repeat of nested loop here ?>

    <tr>
      <td nowrap="nowrap" class="MILtd MILfont-small">

      <?php
      $begin_date_test = $row_Recordset1['begin_date'];
      $begin_date_meaningless = '01-01';
      $begin_date_year = substr($begin_date_test, 0, 4);
      if (strpos($begin_date_test, $begin_date_meaningless) !== false) {
          echo $begin_date_year;
      } else {
          echo $row_Recordset1['begin_date'];
      }
      ?>

      &nbsp;</td>

      <td class="MILtd MILfont-small MILlink" nowrap><a href="../report/report.php?filed_by=<?php echo $row_Recordset1['filed_by']; ?>" target="_top"><?php echo trim($row_Recordset1['filed_by']); ?></a></td>
      <?php
	$flight_id_clean = preg_replace('/\s\s+/', '', $row_Recordset1['filed_by']);
    $flight_id_cleaner = preg_replace('/[-._()]/', '', $flight_id_clean);
	$flight_id_lower = strtolower($flight_id_cleaner);
	?>

      <td align="right" class="MILtd MILfont-small" nowrap>

      <?php if (($row_Recordset1['scale_1'] <> '') && ($row_Recordset1['scale_1'] <> '0'))  {   ?>
	      1:<?php echo number_format($row_Recordset1['scale_1']);  }
                      else { ?>
             &nbsp;
      <?php } ?>
      <?php if (($row_Recordset1['scale_2'] <> '') && ($row_Recordset1['scale_2'] <> '0'))  {   ?>
	  <br/>
          1:<?php echo number_format($row_Recordset1['scale_2']);  }
	  if (($row_Recordset1['scale_3'] <> '') && ($row_Recordset1['scale_3'] <> '0')) {   ?>
	      <br/>
          1:<?php echo number_format($row_Recordset1['scale_3']);  }
	  ?>

      </td>

<?php
include("../common_code/include_report_column_index.php");
?>

<?php
include("../common_code/include_report_column_digital_frames.php");
?>

    </tr>

    <?php
    	if ($first_region_display) {
        $first_region_display = false;
	    }
	?>

    <?php } while ($row_Recordset1 = mysql_fetch_assoc($Recordset1)); ?>
</table>

<div class="MILclear-both">
<br>
</div>

<?php
include("../common_code/include_MIL_footer.php");
?>

</body>
</html>
<?php
mysql_free_result($Recordset1);
?>



